package model.core;

public final class Configuration {

    // Application et fenêtre
    public static final String APPLICATION_NAME = "NipponMasterChef";
    public static final int APPLICATION_DEFAULT_WIDTH = 1008;
    public static final int APPLICATION_DEFAULT_HEIGHT = 750;

    // Vues
    public static final String MAIN_MENU_VIEW = "/fxml/MainMenu.fxml";
    public static final String GAME_VIEW = "/fxml/GameView.fxml";
    public static final String SAVE_MENU_VIEW = "/fxml/SaveMenu.fxml";
    public static final String SCOREBOARD_VIEW = "/fxml/Score.fxml";
    public static final String SHOP_MENU_VIEW = "/fxml/PickaxeShop.fxml";

}
