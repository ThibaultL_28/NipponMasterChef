package model.entity;

import java.io.*;
import java.util.ArrayList;

/**
 * Classe boisson
 */
public class Boisson implements Serializable{

    private String nom;
    private int argentBoisson;
    private String boissonPath;

    /**
     * Constructeur de la classe boisson
     * @param nom : nom de la boisson type String
     * @param argentBoisson : argent que rapporte la vente de la boisson type int
     * @param boissonPath : chemin vers l'image de la boisson
     */
    public Boisson(String nom, int argentBoisson, String boissonPath) {

        this.nom = nom;
        this.argentBoisson = argentBoisson;
        this.boissonPath = boissonPath;

    }

    /**
     * Méthode statique permettant de crée toutes les boissons utilisées par le jeu.
     * Sauvegarde dans un fichier boissons.txt pour pouvoir récuprer les boissons
     */
    public static ArrayList<Boisson> creeAllBoissons(){

        ArrayList<Boisson> boissons = new ArrayList<>();

        try {

            FileOutputStream f = new FileOutputStream(new File("src/save/boisson.txt"));
            ObjectOutputStream oos = new ObjectOutputStream(f);

            Boisson soda = new Boisson("soda", 10, "file:ressources/fxml/images/soda.png");
            Boisson sake = new Boisson("sake", 10, "file:ressources/fxml/images/sake.png");
            Boisson eau = new Boisson("eau", 10, "file:ressources/fxml/images/verreEau.png");

            // Le oos.write(3) permet de spécifier qu'il y à 3 boissons dans le fichier
            oos.write(3);
            oos.writeObject(soda);
            oos.writeObject(sake);
            oos.writeObject(eau);

            boissons.add(soda);
            boissons.add(sake);
            boissons.add(eau);

            oos.close();
        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        // test de l'enregistrement réussi avec un affichage des boissons
        Object o = new Object();
        try {
            FileInputStream f = new FileInputStream(new File("src/save/boisson.txt"));
            ObjectInputStream oos = new ObjectInputStream(f);
            int nbObject = oos.read();
            for (int i = 0; i < nbObject; i++) {
                o = oos.readObject();
                System.out.println(o);
            }
            oos.close();
        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        return boissons;
    }

    /**
     * @return Le nom de la boisson type String
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return Le chemin de la boisson type String
     */
    public String getBoissonPath() {
        return boissonPath;
    }

    /**
     *
     * @return L'argent que rapporte la vente de la boisson
     */
    public int getArgentBoisson() {
        return argentBoisson;
    }

    /**
     *
     * @return Le nom de la boisson et l'argent qu'elle rapporte
     */
    @Override
    public String toString() {
        return "Boisson{" +
                "nom='" + nom + '\'' +
                ", pointBoisson=" + argentBoisson +
                '}';
    }


    /**
     * Permet de comparer 2 boissons en fonction de leur nom
     * @param o
     * @return true ou false en fonction de la comparaison des noms
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Boisson boisson = (Boisson) o;
        return nom.equals(boisson.nom);
    }


    /**
     * Permet de crée des boissons pour le jeu
     * Ne sera pas utilisé par les joueursn utile seulement pour
     * le développement
     * @param args
     */
    public static void main(String[] args) {
        creeAllBoissons();
    }


}
