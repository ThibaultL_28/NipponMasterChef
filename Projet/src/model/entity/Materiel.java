package model.entity;

import java.io.*;
import java.util.ArrayList;

/**
 * Classe matériel
 */
public class Materiel implements Serializable {


    private String nom;
    private boolean propre;
    private int tempsNettoyage;

    /**
     * Constructeur de la classe Matériel
     * @param nom nom du matériel
     * @param propre boolean sur la propreté du matériel
     * @param tempsNettoyage temps de nettoyage en seconde
     */
    public Materiel(String nom, boolean propre, int tempsNettoyage) {
        this.nom = nom;
        this.propre = propre;
        this.tempsNettoyage = tempsNettoyage;
    }

    /**
     * Création de tout les matériels et enregistrement dans un fichier txt
     */
    public static ArrayList<Materiel> creeAllMateriels(){

        ArrayList<Materiel> materiels = new ArrayList<>();

        try {

            FileOutputStream f = new FileOutputStream(new File("src/save/materiel.txt"));
            ObjectOutputStream oos = new ObjectOutputStream(f);

            Materiel cuiseurRiz = new Materiel("cuiseurRiz", true, 10);
            Materiel friteuse = new Materiel("friteuse", true, 15);
            Materiel couteau = new Materiel("couteau", true, 10);

            oos.write(3);
            oos.writeObject(cuiseurRiz);
            oos.writeObject(friteuse);
            oos.writeObject(couteau);

            materiels.add(cuiseurRiz);
            materiels.add(friteuse);
            materiels.add(couteau);


            oos.close();

        }

        catch (Exception e) { System.out.println("Erreur "+e);}

        Object o = new Object();
        try {
            FileInputStream f = new FileInputStream(new File("src/save/materiel.txt"));
            ObjectInputStream oos = new ObjectInputStream(f);
            int nbObject = oos.read();
            for (int i = 0; i < nbObject; i++) {
                o = oos.readObject();
                System.out.println(o);
            }

            oos.close();
        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        return materiels;
    }

    /**
     * @return nom du matériel type String
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return true si le matériel est propre false sinon
     */
    public boolean isPropre() {
        return propre;
    }

    /**
     * @return temps de nettoyage en seconde
     */
    public int getTempsNettoyage() {
        return tempsNettoyage;
    }

    /**
     * @param propre set propreté du matériel
     */
    public void setPropre(boolean propre) {
        this.propre = propre;
    }


    @Override
    public String toString() {
        return "Materiel{" +
                "nom='" + nom + '\'' +
                ", propre=" + propre +
                ", tempsNettoyage=" + tempsNettoyage +
                '}';
    }

    /**
     * Pour crée tout les matériels utilisé seulement en développement
     * @param args
     */
    public static void main(String[] args) {

        creeAllMateriels();

    }


}
