package model.entity;

import java.io.*;
import java.util.ArrayList;

/**
 * Classe recette
 */
public class Recette implements Serializable {

    private ArrayList<Ingredient> ingredients;
    private String nom;
    private ArrayList<Ingredient> aCuire;
    private ArrayList<Ingredient> ordreAssemblageAttendu;
    private ArrayList<Materiel> materiels;
    private int argentRecette;
    // Attention au format exemple "file:ressources/fxml/images/Nigiri.jpg" ne pas oublier le file devant
    private String recettePath;


    /**
     * Constructeur de la classe recette
     * @param ingredients ingrédients de la recette
     * @param nom nom de la recette
     * @param aCuire ingrédients a cuire
     * @param ordreAssemblageAttendu assemblage de la recette
     * @param materiels matériels utiles à la recette
     * @param argentRecette argent que rapporte la recette
     * @param recettePath chemin vers la recette
     */
    public Recette(ArrayList<Ingredient> ingredients, String nom, ArrayList<Ingredient> aCuire, ArrayList<Ingredient> ordreAssemblageAttendu, ArrayList<Materiel> materiels, int argentRecette, String recettePath) {
        this.ingredients = ingredients;
        this.nom = nom;
        this.aCuire = aCuire;
        this.ordreAssemblageAttendu = ordreAssemblageAttendu;
        this.materiels = materiels;
        this.argentRecette = argentRecette;
        this.recettePath = recettePath;
    }

    /**
     * Permet de crée toutes les recettes
     */
    public static ArrayList<Recette> creeAllRecettes(){
        //Pour les tests
        ArrayList<Recette> recettes = new ArrayList<>();

        try {

            FileOutputStream f = new FileOutputStream(new File("src/save/recette.txt"));
            ObjectOutputStream oos = new ObjectOutputStream(f);

            // Création d'ingrédients pour les recettes
            Ingredient riz = new Ingredient("riz", 5, true, true);
            Ingredient saumon = new Ingredient("saumon", 0, true, true);
            Ingredient alge = new Ingredient("algue", 0, true, true);
            Ingredient crevette = new Ingredient("ebifry", 5, true, true);


            ArrayList<Ingredient> ingredientsNigiri = new ArrayList<>();
            ingredientsNigiri.add(riz);
            ingredientsNigiri.add(saumon);

            ArrayList<Ingredient> ingredientsOnigiri = new ArrayList<>();
            ingredientsOnigiri.add(alge);
            ingredientsOnigiri.add(riz);

            ArrayList<Ingredient> ingredientsEbiFry = new ArrayList<>();
            ingredientsEbiFry.add(crevette);

            ArrayList<Ingredient> ingredientsSashimi = new ArrayList<>();
            ingredientsSashimi.add(saumon);

            ArrayList<Ingredient> ordreNigiri = new ArrayList<>();
            ordreNigiri.add(riz);
            ordreNigiri.add(saumon);

            ArrayList<Ingredient> aCuireNigiri = new ArrayList<>();
            aCuireNigiri.add(riz);

            ArrayList<Ingredient> aCuireOnigiri = new ArrayList<>();
            aCuireOnigiri.add(riz);

            Recette nigiri = new Recette(ingredientsNigiri, "nigiri", aCuireNigiri, ordreNigiri, null, 30, "file:ressources/fxml/images/Nigiri.jpg");
            Recette onigiri = new Recette(ingredientsOnigiri, "onigiri", aCuireOnigiri, ingredientsOnigiri, null, 20, "file:ressources/fxml/images/Onigiri.png");
            Recette ebiFry = new Recette(ingredientsEbiFry, "ebifry", ingredientsEbiFry, ingredientsEbiFry, null, 10, "file:ressources/fxml/images/Ebifry2.jpg");
            Recette sashimi = new Recette(ingredientsSashimi, "sashimi", null, ingredientsSashimi, null, 10, "file:ressources/fxml/images/Sashimi.jpg");

            oos.write(4);
            oos.writeObject(nigiri);
            oos.writeObject(onigiri);
            oos.writeObject(ebiFry);
            oos.writeObject(sashimi);
            oos.close();

            //POur les test
            recettes.add(nigiri);
            recettes.add(onigiri);
            recettes.add(ebiFry);
            recettes.add(sashimi);


        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        Object o = new Object();
        try {
            FileInputStream f = new FileInputStream(new File("src/save/recette.txt"));
            ObjectInputStream oos = new ObjectInputStream(f);
            int nbObject = oos.read();
            for (int i = 0; i < nbObject; i++) {
                o = oos.readObject();
                System.out.println(o);
            } oos.close();
        } catch (Exception e) { System.out.println("Erreur "+e);}

        return recettes;

    }


    /**
     * @return nom de la recette type String
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return ordre les ingrédients tels que l'on attend qu'ils soient
     */
    public ArrayList<Ingredient> getOrdreAssemblageAttendu() {
        return ordreAssemblageAttendu;
    }

    /**
     * @return Argent que rapporte la recette
     */
    public int getArgentRecette() {
        return argentRecette;
    }


    /**
     * @return Path de la recette type String
     */
    public String getRecettePath() {
        return recettePath;
    }

    /**
     *
     * @return les valeurs des variables de l'objet en chaine de charactère
     */
    @Override
    public String toString() {
        return "Recette{" +
                "ingredients=" + ingredients +
                ", nom='" + nom + '\'' +
                ", aCuire=" + aCuire +
                ", ordreAssemblageAttendu=" + ordreAssemblageAttendu +
                ", materiels=" + materiels +
                ", pointsRecette=" + argentRecette +
                '}';
    }

    /**
     * Main qui va venir crée toutes les recette
     * @param args
     */
    public static void main(String[] args) {
        creeAllRecettes();
    }
}


