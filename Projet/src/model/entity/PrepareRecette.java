package model.entity;

import java.util.ArrayList;

/**
 * Classe de préparation des recettes
 * Cette classe permettra de vérifier que la
 * reccette qui à été préparer correspond à la recette
 * original
 */
public class PrepareRecette {

    private Recette recetteAPrep;
    private ArrayList<Ingredient> ordreAssemblageEffectue;

    /**
     * @param recetteAPrep : Recette à préparer
     */
    public PrepareRecette(Recette recetteAPrep) {
        this.recetteAPrep = recetteAPrep;
        this.ordreAssemblageEffectue = new ArrayList<>();
    }

    /**
     *
     * @return true ou false, true si la recette est prête et correspond à la recette original
     * false sinon
     */
    public boolean estPrete() {
        if (recetteAPrep.getOrdreAssemblageAttendu().size() != ordreAssemblageEffectue.size()){
            return false;
        }
        for (int i = 0; i < recetteAPrep.getOrdreAssemblageAttendu().size(); i++) {
            if ( ! ordreAssemblageEffectue.get(i).equals(recetteAPrep.getOrdreAssemblageAttendu().get(i))){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param ordreAssemblageEffectue : Ajoute la liste d'assemblage effectué dans la vue Game
     */
    public void setOrdreAssemblageEffectue(ArrayList<Ingredient> ordreAssemblageEffectue) {
        this.ordreAssemblageEffectue = ordreAssemblageEffectue;
    }

    /**
     *
     * @param ingredient : ajoute a la liste d'assemblage un seul ingrédient
     */
    public void addIngredientInListAssemblageEffectue(Ingredient ingredient){
        this.ordreAssemblageEffectue.add(ingredient);
    }

    /**
     *
     * @return récupère l'assemblage effectué
     */
    public ArrayList<Ingredient> getOrdreAssemblageEffectue() {
        return ordreAssemblageEffectue;
    }

}
