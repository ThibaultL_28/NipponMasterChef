package model.entity;

import java.io.*;
import java.util.ArrayList;


/**
 * Classe ingredient
 */
public class Ingredient implements Serializable {

    private int tempsCuisson;
    private boolean estCuit;
    private boolean brule;
    private int tempsCuissonEcoule;
    private boolean enCuisson;
    private boolean continuerCuisson;
    private String nom;
    private boolean estCoupe;


    /**
     * Constructeur de la classe Ingredient
     * @param nom Nom de l'ingrédient
     * @param tempsCuisson Temps de cuisson de l'ingrédient -- 0 s'il ne necessite pas de cuisson
     * @param estCuit Vrai s'il est cuit, faux s'il n'est pas cuit. Si l'ingrédient est par défaut cuit il ne necessite pas de cuisson (exemple : wasabi)
     * @param estCoupe Vrai s'il est coupé, faux s'il n'est pas coupé. Si l'ingrédient est par défaut coupé il ne necessite pas de découpage (exemple : riz)
     */
    public Ingredient(String nom, int tempsCuisson, boolean estCuit, boolean estCoupe) {
        this.nom = nom;
        this.tempsCuisson = tempsCuisson;
        this.estCuit = estCuit;
        this.brule = false;
        this.tempsCuissonEcoule = 0;
        this.continuerCuisson = true;
        this.estCoupe = estCoupe;
    }

    /**
     * Crée tout les ingrédients utilisé dans la vue Game
     */
    public static ArrayList<Ingredient> creeAllIngredients(){

        ArrayList<Ingredient> ingredients = new ArrayList<>();

        try {

            //Sauvegarde les objets Ingrédient dans un fichier txt nommé ingredient.txt
            FileOutputStream f = new FileOutputStream(new File("src/save/ingredient.txt"));
            ObjectOutputStream oos = new ObjectOutputStream(f);

            Ingredient riz = new Ingredient("riz", 5, false, true);
            Ingredient saumon = new Ingredient("saumon", 5, true, false);
            Ingredient alge = new Ingredient("algue", 0, true, true);
            Ingredient ebifry = new Ingredient("ebifry", 5, false, true);


            oos.write(4);
            oos.writeObject(riz);
            oos.writeObject(saumon);
            oos.writeObject(alge);
            oos.writeObject(ebifry);

            ingredients.add(riz);
            ingredients.add(saumon);
            ingredients.add(alge);
            ingredients.add(ebifry);

            oos.close();
        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        Object o = new Object();
        try {
            FileInputStream f = new FileInputStream(new File("src/save/ingredient.txt"));
            ObjectInputStream oos = new ObjectInputStream(f);
            int nbObject = oos.read();
            for (int i = 0; i < nbObject; i++) {
                o = oos.readObject();
                System.out.println(o);
            }




            oos.close();
        }
        catch (Exception e) { System.out.println("Erreur "+e);}

        return ingredients;

    }


    /**
     *
     * @param o
     * @return true si les 2 ingrédients on le même nom, sont tout deux cuit ou pas cuit et sont tout deux coupé ou pas coupé
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return estCuit == that.estCuit &&
                nom.equals(that.nom) && estCoupe == that.estCoupe;
    }


    //Setter et Getter

    /**
     * @return true si l'ingredient est coupé ou s'il necessice pas de coupe, false sinon
     */
    public boolean isEstCoupe() {
        return estCoupe;
    }

    /**
     * @param estCoupe set estCoupe
     */
    public void setEstCoupe(boolean estCoupe) {
        this.estCoupe = estCoupe;
    }

    /**
     * @return temps de cuisson de l'ingredient
     */
    public int getTempsCuisson() {
        return tempsCuisson;
    }

    /**
     * @return true si l'ingredient est cuit ou s'il necessice pas de cuisson, false sinon
     */
    public boolean isEstCuit() {
        return estCuit;
    }

    /**
     * @param estCuit set esCuit
     */
    public void setEstCuit(boolean estCuit) {
        this.estCuit = estCuit;
    }

    /**
     * @return true si l'ingrédient est en cuisson, false sinon
     */
    public boolean isEnCuisson() {
        return enCuisson;
    }

    /**
     * @return le nom de l'ingredient, type String
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return les propriétés de l'ingrédient en chaine de charactère
     */
    @Override
    public String toString() {
        return "Ingredient{" +
                "tempsCuisson=" + tempsCuisson +
                ", estCuit=" + estCuit +
                ", brule=" + brule +
                ", tempsCuissonEcoule=" + tempsCuissonEcoule +
                ", enCuisson=" + enCuisson +
                ", continuerCuisson=" + continuerCuisson +
                ", nom='" + nom + '\'' +
                ", estCoupe=" + estCoupe +
                '}';
    }


    /**
     * Va crée tout les ingrédients utilisés dans la vue Game.
     * Utilisé pour le développement
     * @param args
     */
    public static void main(String[] args) {

        creeAllIngredients();

    }
}


