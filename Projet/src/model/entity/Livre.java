package model.entity;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Permettra de crée les différents livres (livre de recettes, matériels et ingrédients)
 * @param <T>
 */
public class Livre<T> {

    private ArrayList<T> livre;

    /**
     * recette, materiel ou ingrédient
     * @param nomDuLivre
     */
    public Livre(String nomDuLivre) {

        this.livre = new ArrayList<T>();

        Object o = new Object();
        try {

            FileInputStream f = new FileInputStream(("src/save/"+nomDuLivre+".txt"));
            ObjectInputStream oos = new ObjectInputStream(f);
            int nbObject = oos.read();
            for (int i = 0; i < nbObject; i++) {
                o = oos.readObject();
                livre.add((T)o);
            } oos.close();
        } catch (Exception e) { System.out.println("Erreur "+e);}
    }

    /**
     * @return récupère le livre
     */
    public ArrayList<T> getLivre() {
        return livre;
    }





}


