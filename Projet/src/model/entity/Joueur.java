package model.entity;


/**
 * Classe joueur
 */
public class Joueur {

    private String nom;
    private int argent;

    /**
     * Constructeur de la classe Joueur
     * @param nom Nom du joueur
     * @param argent Argent du joueur
     */
    public Joueur(String nom, int argent) {
        this.nom = nom;
        this.argent = argent;
    }


    /**
     * @return les propriétés du joueur en chaine de charactère
     */
    @Override
    public String toString() {
        return "Joueur{" +
                "nom='" + nom + '\'' +
                ", argent=" + argent +
                '}';
    }

    /**
     * @return le nom type String
     */
    public String getNom() {
        return nom;
    }


    /**
     * @return l'argent du joueur type String
     */
    public int getArgent() {
        return argent;
    }


    /**
     * @param argent set l'argent du joueur
     */
    public void setArgent(int argent) {
        this.argent = argent;
    }





}
