package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Vue de lancement du jeu
 */
public class MenuGame {

    @FXML
    private Button btnGame;


    /**
     * Amène au menu principal
     * @throws IOException
     */
    @FXML
    private void goToGameMenu() throws IOException{
        if(Launch.isFr){
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnGame.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnGame.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }

}
