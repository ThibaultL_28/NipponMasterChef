package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Vue paramètre généraux du jeu
 */
public class ParamMenu implements Initializable {

        @FXML
        private Button closeButton;

        @FXML
        private Button btnFr;

        @FXML
        private Button btnEng;

        @FXML
        private Button btnValide;

        @FXML
        private CheckBox musiqueCB;


    /**
     * Initialise la vue des paramètres (ici la checkbox de la musique et la selection de la langue)
     * @param url
     * @param resourceBundle
     */
    @Override
        public void initialize(URL url, ResourceBundle resourceBundle) {
            if (Launch.mediaPlayer.isMute()){
                musiqueCB.setSelected(false);
            } else {
                musiqueCB.setSelected(true);
            }
            if (Launch.isFr){
                btnFr.getStyleClass().add("btn-selected");
            } else {
                btnEng.getStyleClass().add("btn-selected");
            }
        }

    /**
     * Amène au menu principal
     * @throws IOException
     */
    @FXML
        private void toMainMenu() throws IOException {
            Scene nouvelleScene;
            Stage nouvelleFenetre = new Stage();
            if(Launch.isFr) {
                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));

                nouvelleFenetre.setScene(nouvelleScene);
                nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                //Ferme la fenêtre courrante
                Stage stage = (Stage) closeButton.getScene().getWindow();
                stage.close();

            }
            else {
                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));

                nouvelleFenetre.setScene(nouvelleScene);
                nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                //Ferme la fenêtre courrante
                Stage stage = (Stage) closeButton.getScene().getWindow();
                stage.close();

            }
            nouvelleFenetre.show();
        }


    /**
     * Change le css en fonction de la langue
     */
    public void inLanguageEng(){
            Launch.isFr = false;
            btnEng.getStyleClass().add("btn-selected");
            btnFr.getStyleClass().remove("btn-selected");
        }

    /**
     * Change le css en fonction de la langue
     */
    public void enLangueFr(){
            Launch.isFr = true;
            btnFr.getStyleClass().add("btn-selected");
            btnEng.getStyleClass().remove("btn-selected");
        }

    /**
     * Va venir valider les modification effectuer dans le menu de paramètres
     * @throws IOException
     * @throws URISyntaxException
     */
    public void validerModif() throws IOException, URISyntaxException {
            //todo Rajouter la musique  (Bouton CheckBox Valider)

            if(musiqueCB.isSelected() && Launch.mediaPlayer.isMute()){
                Launch.startMusique();
            } else if (!musiqueCB.isSelected()){
                Launch.stopMusique();
            }


            Scene nouvelleScene;
            Stage nouvelleFenetre = new Stage();
            if(Launch.isFr) {
                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/ParamMenu.fxml")));

                nouvelleFenetre.setScene(nouvelleScene);
                nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                //Ferme la fenêtre courrante
                Stage stage = (Stage) closeButton.getScene().getWindow();
                stage.close();

            }
            else {
                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/ParamMenuAnglais.fxml")));

                nouvelleFenetre.setScene(nouvelleScene);
                nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                //Ferme la fenêtre courrante
                Stage stage = (Stage) closeButton.getScene().getWindow();
                stage.close();

            }
            nouvelleFenetre.show();
        }


}


