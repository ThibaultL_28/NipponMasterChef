package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Vue recette ebifry
 */
public class EbyFryRecette {

    @FXML private Button Retour;
    @FXML private Button MenuPrincipal;

    /**
     * Amène vers la vue main menu
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {
        // va venir vérifier si on doit charger la page en anglais ou français
        if(Launch.isFr){
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) MenuPrincipal.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) MenuPrincipal.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }

    /**
     * Amène vers la vue du livre des recettes
     * @throws IOException
     */
    @FXML
    private void toBookMenu() throws IOException {
        // va venir vérifier si on doit charger la page en anglais ou français
        if (Launch.isFr) {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/BookMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Retour.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/BookMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Retour.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }

}
