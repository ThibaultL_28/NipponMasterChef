package view;

import launcher.Launch;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Menu principal
 */
public class MainMenu implements Initializable {


    @FXML private Button changeNameButton;
    @FXML private TextField changeNameTextField;
    @FXML private Button toGame;
    @FXML private Button toBook;
    @FXML private Button toScore;
    @FXML private Button toParam;

    /**
     * Va venir prendre en compte le nom du joueur si il a été renseigné
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (!Launch.nomJoueur.equals("Inconnu")){
            changeNameButton.setText(Launch.nomJoueur);
        }
    }

    /**
     * Amène a la vue de selection du niveau
     * @throws IOException
     */
    @FXML
    private void toGame() throws IOException {
        if(Launch.isFr) {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/LvlMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toGame.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }else
        {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/LvlMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toGame.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }

    /**
     * Amène au livre des recettes
     * @throws IOException
     */
    @FXML
    private void toBook() throws IOException {
        if(Launch.isFr) {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/BookMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toBook.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }else
        {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/BookMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toGame.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }


    }

    /**
     * Amène a la vue des scores
     * @throws IOException
     */
    @FXML
    private void toScore() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if(Launch.isFr) {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/ScoreMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toScore.getScene().getWindow();
            stage.close();

        }else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/ScoreMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toGame.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }

    /**
     * Amène à la vue des paramètres généraux
     * @throws IOException
     */
    @FXML
    private void toParam() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if(Launch.isFr) {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/ParamMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toParam.getScene().getWindow();
            stage.close();

        }
        else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/ParamMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) toGame.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }


    public void changeName(ActionEvent actionEvent) {
        changeNameButton.visibleProperty().setValue(false);
        changeNameTextField.visibleProperty().setValue(true);
    }


    /**
     * Change de nom, pris en validation après avoir appuier sur entrée
     */
    public void validNom(KeyEvent dragEvent) {
        if (dragEvent.getCode().equals(KeyCode.ENTER)) {
            changeNameButton.setText(changeNameTextField.getText());
            changeNameButton.setVisible(true);
            changeNameTextField.setVisible(false);
            Launch.nomJoueur = changeNameTextField.getText();
        }
    }


}
