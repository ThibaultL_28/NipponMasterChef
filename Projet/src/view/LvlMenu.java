package view;

import launcher.Launch;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.entity.Recette;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Vue selection du niveau
 */
public class LvlMenu implements Initializable {

    /**
     * Va venir activé les autres niveaux si le niveau 1 a été terminé
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (Launch.niveau1Termine){
            niveau2.setDisable(false);
            niveauPerso.setDisable(false);
        }
    }

    /**
     * Lancement niveau 1 avec réinitialisation des paramètres
     */
    @FXML private Button niveau1;
    public void niveau1(ActionEvent actionEvent) throws IOException {

        //On remet les paramètres par défaut
        Launch.boissonActive = false;

        Recette.creeAllRecettes();

        Launch.nigiri = true;
        Launch.onigiri = true;
        Launch.ebifry = true;
        Launch.sashimi = true;

        Launch.soda = true;
        Launch.sake = true;
        Launch.eau = true;

        Launch.nbClient = 3;

        Launch.temps = 120;


        if(Launch.isFr){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Game.fxml"));
            // Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/Game.fxml")));
            Scene nouvelleScene = new Scene(loader.load());
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) niveau1.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/english/GameAnglais.fxml"));
            Scene nouvelleScene = new Scene(loader.load());
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) niveau1.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }

    /**
     * Lancement niveau 2 avec réinitialisation des paramètres
     */
    @FXML private Button niveau2;
    public void niveau2(ActionEvent actionEvent) throws IOException {

        //On remet les paramètres par défaut
        Launch.boissonActive = true;

        Recette.creeAllRecettes();

        Launch.nigiri = true;
        Launch.onigiri = true;
        Launch.ebifry = true;
        Launch.sashimi = true;

        Launch.soda = true;
        Launch.sake = true;
        Launch.eau = true;

        Launch.nbClient = 3;

        Launch.temps = 120;

        if(Launch.isFr){
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/Game.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) niveau2.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/english/GameAnglais.fxml"));
            Scene nouvelleScene = new Scene(loader.load());
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) niveau2.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }


    }

    /**
     * Lancement de la page pour personaliser le niveau personnalisé
     */
    @FXML private Button niveauPerso;
    public void niveauPerso() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if (Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/PersonnalizeMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) niveauPerso.getScene().getWindow();
            stage.close();

        }
        else {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/PersonnalizeMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) niveauPerso.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();

    }



    @FXML
    private Button retour;

    /**
     * Retour au main menu
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {
        if (Launch.isFr){
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            //Ferme la fenêtre courrante
            Stage stage = (Stage) retour.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) retour.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }


}
