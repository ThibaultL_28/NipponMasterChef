package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Vue livre des recettes
 */
public class BookMenu {

    @FXML private Button Onigiri;
    @FXML private Button Sashimi;
    @FXML private Button Nigiri;
    @FXML private Button Ebyfry;
    @FXML private Button retour;

    /**
     * Amène vers la vue de la recette de l'onigiri
     * @throws IOException
     */
    @FXML
    private void toOnigiri() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        // va venir vérifier si on doit charger la page en anglais ou français
        if(Launch.isFr) {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/recettes/OnigiriRecette.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Onigiri.getScene().getWindow();
            stage.close();

        }
        else
            {
                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/recipes/OnigiriRecetteAnglais.fxml")));
                nouvelleFenetre.setScene(nouvelleScene);
                nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                Stage stage = (Stage) Onigiri.getScene().getWindow();
                stage.close();

            }
        nouvelleFenetre.show();
    }

    /**
     * Amène vers la vue recette du sashimi
     * @throws IOException
     */
    @FXML
    private void toSashimi() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        // va venir vérifier si on doit charger la page en anglais ou français
        if(Launch.isFr) {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/recettes/SashimiRecette.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Sashimi.getScene().getWindow();
            stage.close();

        }else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/recipes/SashimiRecetteAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Sashimi.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }

    /**
     * Amène vers la vue recette Nigiri
     * @throws IOException
     */
    @FXML
    private void toNigiri() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        // va venir vérifier si on doit charger la page en anglais ou français
        if(Launch.isFr) {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/recettes/NigiriRecette.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Nigiri.getScene().getWindow();
            stage.close();

        }
        else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/recipes/NigiriRecetteAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Nigiri.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }

    /**
     * Amène vers la vue recette ebyfry
     * @throws IOException
     */
    @FXML
    private void toEbyfry() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        // va venir vérifier si on doit charger la page en anglais ou français
        if(Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/recettes/EbyFryRecette.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Ebyfry.getScene().getWindow();
            stage.close();

        }
        else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/recipes/EbyFryRecetteAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Ebyfry.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }


    /**
     * Amène vers la vue main menu
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        // va venir vérifier si on doit charger la page en anglais ou français

        if (Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());
            Stage stage = (Stage) retour.getScene().getWindow();
            stage.close();

        }
        else
        {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) retour.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }
}
