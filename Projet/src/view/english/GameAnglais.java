package view.english;

import launcher.Launch;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.entity.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;

public class GameAnglais implements Initializable {


    /*
    Variables FXML
     */

    // AnchorPane
    public AnchorPane ap;

    //Label
    @FXML
    public Label compteur;
    @FXML
    public Label cuiseurRiz;
    @FXML
    public Label couteau;
    @FXML
    public Label friteuse;

    //ListView
    @FXML
    public ListView assemblage;
    @FXML
    public ListView boisson;

    //Image View
    @FXML
    public ImageView ingredientSelection;
    @FXML
    public ImageView recette1;
    @FXML
    public ImageView recette2;
    @FXML
    public ImageView recette3;
    @FXML
    public ImageView ingredientSelection1;
    @FXML
    public ImageView ingredientSelection2;
    @FXML
    public ImageView perso1;
    @FXML
    public ImageView perso2;
    @FXML
    public ImageView perso3;
    @FXML
    public ImageView boisson1;
    @FXML
    public ImageView boisson2;
    @FXML
    public ImageView boisson3;
    @FXML
    public ImageView soda;
    @FXML
    public ImageView sake;
    @FXML
    public ImageView eau;
    @FXML
    public ImageView imgCuiseur;
    @FXML
    public ImageView imgCouteau;
    @FXML
    public ImageView imgFriteuse;

    //Button
    @FXML
    public Button decoupe;
    @FXML
    public Button decoupe1;
    @FXML
    public Button decoupe2;
    @FXML
    public Button assemble;
    @FXML
    public Button assemble1;
    @FXML
    public Button assemble2;
    @FXML
    public Button cuire;
    @FXML
    public Button cuire1;
    @FXML
    public Button cuire2;
    @FXML
    public Button laverCuiseur;
    @FXML
    public Button laverCouteau;
    @FXML
    public Button frire;
    @FXML
    public Button laverFriteuse;
    @FXML
    public Button frire1;
    @FXML
    public Button frire2;
    @FXML
    private Button btnRetour;
    @FXML
    private Button bookRecette;
    //Ellipse
    @FXML
    public Ellipse bulle1;
    @FXML
    public Ellipse bulle2;
    @FXML
    public Ellipse bulle3;
    @FXML
    public ImageView imgServir;
    @FXML
    public ImageView imgAssiete;

    public ProgressIndicator progressBar;
    public ProgressIndicator progressBar1;
    public ProgressIndicator progressBar2;


    /*
    Autres variables
     */

    public Livre<Recette> livreRecettes;
    public Livre<Ingredient> livreIngredients;
    public Livre<Boisson> livreBoisson;
    public Livre<Materiel> livreMateriel;

    public ArrayList<Ingredient> listAssemblage = new ArrayList<>();
    public ArrayList<Boisson> listAssemblageBoisson = new ArrayList<>();

    public ArrayList<Recette> recettesEnAttente = new ArrayList<>();
    public ArrayList<Boisson> boissonsEnAttente = new ArrayList<>();

    public Ingredient[] listSelection = new Ingredient[3];
    public Boisson[] listSelectionBoisson = new Boisson[3];

    public Recette recetteEnCours;
    public Boisson boissonEnCours;

    public PrepareRecette prepareRecette;

    public int temps = Launch.temps;
    public int tempsLavageCuiseur;
    public int tempsLavageCouteau;
    public int tempsLavageFriteuse;
    public int tempsCuissonRiz;
    public int tempsDecoupe;
    public int tempsFriture;
    public boolean enPause = false;
    public boolean enCuissonRiz = false;
    public boolean enDecoupe = false;
    public boolean enFriture = false;
    public Joueur joueur;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        compteur.setText("Time : " + temps);
        tempsNiveau();
        livreRecettes = new Livre<>("recette");
        livreIngredients = new Livre<>("ingredient");
        livreMateriel = new Livre<>("materiel");
        joueur = new Joueur(Launch.nomJoueur, 0);

        //Pour le niveau personalisé
        if (!Launch.sashimi){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("sashimi"));
        }if (!Launch.onigiri){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("onigiri"));
        }if (!Launch.ebifry){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("ebifry"));
        }if (!Launch.nigiri){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("nigiri"));
        }


        if (Launch.nbClient == 3){
            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));
            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));
            recette2.setImage(new Image(recettesEnAttente.get(0).getRecettePath()));
            recette3.setImage(new Image(recettesEnAttente.get(1).getRecettePath()));
        } else if (Launch.nbClient == 2){
            perso3.setVisible(false);
            bulle3.setVisible(false);

            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));

            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));
            recette2.setImage(new Image(recettesEnAttente.get(0).getRecettePath()));

        } else if (Launch.nbClient == 1){
            perso3.setVisible(false);
            bulle3.setVisible(false);
            perso2.setVisible(false);
            bulle2.setVisible(false);

            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));

        }


        if (Launch.boissonActive) {

            //activation des boissons
            soda.setDisable(false);
            eau.setDisable(false);
            sake.setDisable(false);
            livreBoisson = new Livre<>("boisson");

            //Pour le niveau personalisé
            if (!Launch.soda){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("soda"));
            }if (!Launch.sake){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("sake"));
            }if (!Launch.eau){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("eau"));
            }

            if (Launch.nbClient == 3){
                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));
                boisson2.setImage(new Image(boissonsEnAttente.get(0).getBoissonPath()));
                boisson3.setImage(new Image(boissonsEnAttente.get(1).getBoissonPath()));
            } else if (Launch.nbClient == 2){

                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));
                boisson2.setImage(new Image(boissonsEnAttente.get(0).getBoissonPath()));

            } else if (Launch.nbClient == 1){

                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));

            }



        }

        prepareRecette = new PrepareRecette(recetteEnCours);

        tempsLavageCuiseur = getMaterielByName("cuiseurRiz").getTempsNettoyage();
        tempsLavageCouteau = getMaterielByName("couteau").getTempsNettoyage();
        tempsLavageFriteuse = getMaterielByName("friteuse").getTempsNettoyage();
        tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();
        tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();
        tempsFriture = getIngredientByName("ebifry").getTempsCuisson(); 




        /* Exemple animation pour les personnages
        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(2), perso1);
        translateAnimation.setByX(-50);
        translateAnimation.setByY(50);
        translateAnimation.play();

         */


    }

    public Recette getRandomRecette(ArrayList<Recette> list) {
        Random r = new Random();
        int randomNumber = r.nextInt(list.size());
        return list.get(randomNumber);
    }

    public Boisson getRandomBoisson(ArrayList<Boisson> list) {
        Random r = new Random();
        int randomNumber = r.nextInt(list.size());
        return list.get(randomNumber);
    }

    public void tempsNiveau() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        temps--;
                        compteur.setText("Time : " + temps);
                        if (temps == 0) {

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setContentText("Lost back to main menu");
                            alert.showAndWait();

                            Scene nouvelleScene = null;
                            try {
                                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Stage nouvelleFenetre = new Stage();
                            nouvelleFenetre.setScene(nouvelleScene);
                            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                            Stage stage = (Stage) ap.getScene().getWindow();
                            stage.close();

                            nouvelleFenetre.show();
                        }
                    }
                };
                while (!enPause) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void selectionIngredient(MouseEvent mouseEvent) {

        ImageView imageView = (ImageView) mouseEvent.getSource();
        Image image = imageView.getImage();
        if (ingredientSelection.getImage() == null) {
            ingredientSelection.setImage(image);
            ingredientSelection.setAccessibleText(imageView.getAccessibleText());

            //todo bug sur les ingrédients a finir (normalement c bon) 
            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[0] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }

            //listSelection[0] = (getIngredientByName(imageView.getAccessibleText()));
            if (listSelection[0] == null) {
                listSelection[0] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[0] = getBoissonByName(imageView.getAccessibleText());
            }

        } else if (ingredientSelection1.getImage() == null) {
            ingredientSelection1.setImage(image);
            ingredientSelection1.setAccessibleText(imageView.getAccessibleText());
            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[1] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }
            //listSelection[1] = (getIngredientByName(imageView.getAccessibleText()));
            if (listSelection[1] == null) {
                listSelection[1] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[1] = getBoissonByName(imageView.getAccessibleText());
            }
        } else if (ingredientSelection2.getImage() == null) {
            ingredientSelection2.setImage(image);
            ingredientSelection2.setAccessibleText(imageView.getAccessibleText());
            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[2] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }
            //
            listSelection[2] = (getIngredientByName(imageView.getAccessibleText()));
            if (listSelection[2] == null) {
                listSelection[2] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[2] = getBoissonByName(imageView.getAccessibleText());
            }
        }
        //Pour récupérer le nom des ingrédients
        System.out.println(imageView.getAccessibleText());

    }

    public void desassembler(MouseEvent contextMenuEvent) {

        int index = assemblage.getSelectionModel().getSelectedIndex();
        assemblage.getItems().remove(index);
        listAssemblage.remove(index);

    }

    public void desassemblerBoisson(MouseEvent mouseEvent) {
        int index = boisson.getSelectionModel().getSelectedIndex();
        boisson.getItems().remove(index);
        listAssemblageBoisson.remove(index);
    }


    //todo correction du bug d'assemblage d'aliment en cuisson ou découpe
    public void assembler(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();
        if (b.getId().equals("assemble")) {
            if (ingredientSelection.getImage() != null) {
                if (!listSelection[0].getNom().equals("null")) {
                    progressBar.setProgress(0);
                    assemblage.getItems().add(ingredientSelection.getAccessibleText());
                    listAssemblage.add(listSelection[0]);
                    listSelection[0] = null;
                    ingredientSelection.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[0]);
                    listSelectionBoisson[0] = null;
                    ingredientSelection.setImage(null);
                }
            }

        } else if (b.getId().equals("assemble1")) {
            if (ingredientSelection1.getImage() != null) {
                if (!listSelection[1].getNom().equals("null")) {
                    progressBar1.setProgress(0);
                    assemblage.getItems().add(ingredientSelection1.getAccessibleText());
                    listAssemblage.add(listSelection[1]);
                    listSelection[1] = null;
                    ingredientSelection1.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection1.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[1]);
                    listSelectionBoisson[1] = null;
                    ingredientSelection1.setImage(null);
                }
            }
        } else {

            if (ingredientSelection2.getImage() != null) {

                if (!listSelection[2].getNom().equals("null")) {
                    progressBar2.setProgress(0);
                    assemblage.getItems().add(ingredientSelection2.getAccessibleText());
                    listAssemblage.add(listSelection[2]);
                    listSelection[2] = null;
                    ingredientSelection2.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection2.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[2]);
                    listSelectionBoisson[2] = null;
                    ingredientSelection2.setImage(null);
                }
            }
        }
    }

    public void decoupe(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();

        if (b.getId().equals("decoupe")) {
            // temps de découpe a faire
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {

                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble.setDisable(true);

                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    decoupeSaumon(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop cut");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/morceauFish2.png"));
                    listSelection[0].setEstCoupe(true);
                    getMaterielByName("couteau").setPropre(false);
                    couteau.setText("Sale");
                    laverCouteau.setDisable(false);

                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {

                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble.setDisable(false);


                    enDecoupe = false;
                    b.setText("Cut");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }

                //ingredientSelection.setAccessibleText("saumon découpé");
            }
        } else if (b.getId().equals("decoupe1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {
                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble1.setDisable(true);


                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    decoupeSaumon(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop cut");
                    /*
                    ingredientSelection1.setImage(new Image("file:ressources/fxml/images/morceauFish2.png"));
                    listSelection[1].setEstCoupe(true);
                    getMaterielByName("couteau").setPropre(false);
                    couteau.setText("Sale");
                    laverCouteau.setDisable(false);

                     */

                } else {
                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble1.setDisable(false);

                    enDecoupe = false;
                    b.setText("Cut");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }

            }
        } else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {
                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble2.setDisable(true);

                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    decoupeSaumon(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop cut");
                    /*
                    ingredientSelection2.setImage(new Image("file:ressources/fxml/images/morceauFish2.png"));
                    listSelection[2].setEstCoupe(true);
                    getMaterielByName("couteau").setPropre(false);
                    couteau.setText("Sale");
                    laverCouteau.setDisable(false);

                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble2.setDisable(false);

                    enDecoupe = false;
                    b.setText("Cut");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }
                //ingredientSelection.setAccessibleText("saumon découpé");
            }
        }
    }

    public void frire(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getSource();
        if (b.getId().equals("frire")) {
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop frying");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[0].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);
                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble.setDisable(false);

                    enFriture = false;
                    b.setText("Fry");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }else if (b.getId().equals("frire1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble1.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop frying");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[0].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);
                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble1.setDisable(false);

                    enFriture = false;
                    b.setText("Fry");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble2.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop frying");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[0].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);
                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble2.setDisable(false);

                    enFriture = false;
                    b.setText("Fry");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }
    }


    public void cuire(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();
        if (b.getId().equals("cuire")) {
            // temps de cuissson a faire
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("riz")) {
                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {

                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble.setDisable(true);


                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop cooking");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[0].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);
                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cook");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }
            }
        } else if (b.getId().equals("cuire1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("riz")) {
                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {
                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble1.setDisable(true);

                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop cooking");

                    /*
                    ingredientSelection1.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[1].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);

                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble1.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cook");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }

            }
        } else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("riz")) {

                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {
                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble2.setDisable(true);

                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop cooking");

                    /*
                    ingredientSelection2.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[2].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);

                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble2.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cook");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }


            }
        }

    }

    public Ingredient getIngredientByName(String nom) {
        for (Ingredient t : livreIngredients.getLivre()) {
            if (t.getNom().equals(nom)) {
                return t;
            }
        }
        return null;
    }

    public Materiel getMaterielByName(String nom) {
        for (Materiel m : livreMateriel.getLivre()) {
            if (m.getNom().equals(nom)) {
                return m;
            }
        }
        return null;
    }

    public Boisson getBoissonByName(String nom) {
        for (Boisson b : livreBoisson.getLivre()) {
            if (b.getNom().equals(nom)) {
                return b;
            }
        }
        return null;
    }

    public void servir(ActionEvent actionEvent) throws IOException {

        boolean pret = true;

        prepareRecette.setOrdreAssemblageEffectue(listAssemblage);

        if (Launch.boissonActive) {
            pret = false;
            if ((!boisson.getItems().isEmpty()) && boissonEnCours.equals(listAssemblageBoisson.get(0)) && boisson.getItems().size() == 1) {
                pret = true;
            }
        }


        if (prepareRecette.estPrete() && pret) {

            if (recette1.isVisible()) {
                if (Launch.boissonActive) {
                    boisson1.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {

                        boissonEnCours = boissonsEnAttente.remove(0);

                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette1.setVisible(false);
                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());

                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(-500);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(-500);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle1.setVisible(false);
                    perso1.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(500);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(500);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));

                if (!recettesEnAttente.isEmpty()) {


                    //todo finir l'animation (bien avancé faire pour les recettes 2 et 3)



                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);
                    /* todo bug sur les éléments cuit ou coupé (fait)
                    for (Ingredient i: listAssemblage) {
                        if (i.getNom().equals("riz")){
                            i.setEstCuit(false);
                        }
                        if (i.getNom().equals("saumon")) {
                            i.setEstCoupe(false);
                        }
                    }

                     */
                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();
                    /*
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Recette réussi");
                    alert.showAndWait();

                     */
                } else {

                    //todo mathilde save nom, argent + temps mis pour le lvl
                    //todo pour récup le temps mis faire Launch.temps - temps

                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", money : " + joueur.getArgent() + "\n");
                    pw.close();



                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Completed level money earned : " + joueur.getArgent());
                    alert.showAndWait();

                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
                //todo passer a la recette suivante (et ensuite adapter a la commande)
            } else if (recette2.isVisible()) {
                if (Launch.boissonActive) {
                    boisson2.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {
                        boissonEnCours = boissonsEnAttente.remove(0);
                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette2.setVisible(false);
                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());

                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(-150);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(-150);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle2.setVisible(false);
                    perso2.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(150);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(150);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));


                if (!recettesEnAttente.isEmpty()) {
                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);
                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();

                    /*
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Recette réussi");
                    alert.showAndWait();

                     */
                } else {
                    //todo mathilde save nom, argent + temps mis pour le lvl
                    //todo pour récup le temps mis faire Launch.temps - temps

                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", money : " + joueur.getArgent() + "\n");
                    pw.close();


                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Completed level money earned : " + joueur.getArgent());
                    alert.showAndWait();

                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
            } else if (recette3.isVisible()) {
                if (Launch.boissonActive) {
                    boisson3.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {
                        boissonEnCours = boissonsEnAttente.remove(0);
                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette3.setVisible(false);
                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());

                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(200);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(200);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle3.setVisible(false);
                    perso3.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(-200);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(-200);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));


                if (!recettesEnAttente.isEmpty()) {
                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);
                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();

                    /*
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Recette réussi");
                    alert.showAndWait();

                     */
                } else {
                    //todo mathilde save nom, argent + temps mis pour le lvl
                    //todo pour récup le temps mis faire Launch.temps - temps

                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", money : " + joueur.getArgent() + "\n");
                    pw.close();

                    Launch.niveau1Termine = true;

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Completed level money earned : " + joueur.getArgent());
                    alert.showAndWait();
                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Wrong recipe");
            alert.showAndWait();
        }
    }

    public void laverCuiseur(ActionEvent actionEvent) {

        laverCuiseur.setDisable(true);
        laverCuiseur.setText("In washing");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCuiseur);
        translateAnimation.setByX(-200);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageCuiseur--;
                        cuiseurRiz.setText("Time : " + tempsLavageCuiseur);
                        if (tempsLavageCuiseur < 0) {
                            cuiseurRiz.setText("clean");
                            getMaterielByName("cuiseurRiz").setPropre(true);
                            tempsLavageCuiseur = getMaterielByName("cuiseurRiz").getTempsNettoyage();
                            //laverCuiseur.setDisable(false);
                            laverCuiseur.setText("Wach cooker");
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageCuiseur > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void laverCouteau(ActionEvent actionEvent) {

        laverCouteau.setDisable(true);
        laverCouteau.setText("In washing");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCouteau);
        translateAnimation.setByX(-300);
        translateAnimation.setByY(-80);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageCouteau--;
                        couteau.setText("Time : " + tempsLavageCouteau);
                        if (tempsLavageCouteau < 0) {
                            couteau.setText("Clean");
                            getMaterielByName("couteau").setPropre(true);
                            tempsLavageCouteau = getMaterielByName("couteau").getTempsNettoyage();
                            // laverCouteau.setDisable(false);
                            laverCouteau.setText("Wach knife");
                            translateAnimation.setByX(300);
                            translateAnimation.setByY(80);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageCouteau > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    public void laverFriteuse(ActionEvent actionEvent) {

        laverFriteuse.setDisable(true);
        laverFriteuse.setText("In washing");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgFriteuse);
        translateAnimation.setByX(-300);
        translateAnimation.setByY(-120);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageFriteuse--;
                        friteuse.setText("Time : " + tempsLavageFriteuse);
                        if (tempsLavageFriteuse < 0) {
                            friteuse.setText("Clean");
                            getMaterielByName("friteuse").setPropre(true);
                            tempsLavageFriteuse = getMaterielByName("friteuse").getTempsNettoyage();
                            // laverCouteau.setDisable(false);
                            laverFriteuse.setText("Wach fryer");
                            translateAnimation.setByX(300);
                            translateAnimation.setByY(120);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageFriteuse > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void cuireRiz(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsCuissonRiz;

        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("In cooking");

        /*
        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCuiseur);
        translateAnimation.setByX(-200);
        translateAnimation.play();
         */

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsCuissonRiz--;
                        // cuiseurRiz.setText("temps : " + tempsLavageCuiseur);
                        if (tempsCuissonRiz < 0 && tempsCuissonRiz > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                            listSelect[0].setEstCuit(true);
                            cuiseurRiz.setText("Dirty");
                            laverCuiseur.setDisable(false);
                            b.setDisable(false);
                            /*
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                             */
                        } else if (tempsCuissonRiz < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }
                            cuiseurRiz.setText("Burnt");
                            // application du malus
                            tempsLavageCuiseur = tempsLavageCuiseur + 5;
                            enCuissonRiz = false;

                            cuire.setDisable(false);
                            cuire1.setDisable(false);
                            cuire2.setDisable(false);

                            b.setText("Cook");
                            tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();
                        }
                    }
                };
                while (enCuissonRiz) {
                    if (nbProgressBar == 1){
                        progressBar.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }else if (nbProgressBar == 2){
                        progressBar1.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }else if (nbProgressBar == 3){
                        progressBar2.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }
                    try {
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    public void decoupeSaumon(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsDecoupe;

        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("In cutting");

        /*
        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCuiseur);
        translateAnimation.setByX(-200);
        translateAnimation.play();
         */

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsDecoupe--;
                        // cuiseurRiz.setText("temps : " + tempsLavageCuiseur);
                        if (tempsDecoupe < 0 && tempsDecoupe > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/morceauFish2.png"));
                            listSelect[0].setEstCoupe(true);
                            couteau.setText("Dirty");
                            laverCouteau.setDisable(false);
                            b.setDisable(false);
                            /*
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                             */
                        } else if (tempsDecoupe < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }

                            couteau.setText("Very dirty");
                            // application du malus
                            tempsLavageCouteau = tempsLavageCouteau + 5;
                            enDecoupe = false;

                            decoupe.setDisable(false);
                            decoupe1.setDisable(false);
                            decoupe2.setDisable(false);

                            b.setText("Cut");
                            tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();
                        }
                    }
                };
                while (enDecoupe) {
                    try {
                        if (nbProgressBar == 1){
                            progressBar.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }else if (nbProgressBar == 2){
                            progressBar1.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }else if (nbProgressBar == 3){
                            progressBar2.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    public void frireEbi(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsFriture;


        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("In Frying");

        /*
        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCuiseur);
        translateAnimation.setByX(-200);
        translateAnimation.play();
         */

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsFriture--;
                        if (tempsFriture < 0 && tempsFriture > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/Ebifry2.jpg"));
                            listSelect[0].setEstCuit(true);
                            friteuse.setText("Dirty");
                            laverFriteuse.setDisable(false);
                            b.setDisable(false);
                            /*
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                             */
                        } else if (tempsFriture < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }
                            friteuse.setText("Burnt");
                            // application du malus
                            tempsLavageFriteuse = tempsLavageFriteuse + 5;
                            enFriture = false;

                            frire.setDisable(false);
                            /* todo les autres boutons frire
                            cuire.setDisable(false);
                            cuire1.setDisable(false);
                            cuire2.setDisable(false);

                             */

                            b.setText("Fry");
                            tempsFriture = getIngredientByName("ebifry").getTempsCuisson();
                        }
                    }
                };
                while (enFriture) {
                    if (nbProgressBar == 1){
                        progressBar.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }else if (nbProgressBar == 2){
                        progressBar1.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }else if (nbProgressBar == 3){
                        progressBar2.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }
                    try {
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Si l'utilisateur clique sur cancel, alors il reste sur le jeu
     * si l'utilisateur clique sur YES, alors il retourne directement au menu du jeu
     * @throws IOException
     */

    @FXML
    private void toMainMenu() throws IOException {

        enPause = true;
        showMessage(Alert.AlertType.CONFIRMATION, null, "Are you sure you want to return to the Main Menu?", ButtonType.CANCEL, ButtonType.YES)
                .filter(bouton -> bouton == ButtonType.YES)
                .ifPresentOrElse(
                        (bouton) -> {
                            try {
                                goInMenu();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        },
                        () -> {
                            enPause = false;
                            tempsNiveau();
                        });
                 /*
                )
                .ifPresent(bouton -> {
                    try {
                        goInMenu();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                  */
    }


    /**
     * Permet d'aller dans le menu après avoir confirmer pour quitter la partie
     * Condition pour le YES dans une autre méthode afin de pouvoir la mettre dans le .ifPresent
     * @throws IOException
     */
    private void goInMenu() throws IOException{
        Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
        Stage nouvelleFenetre = new Stage();
        nouvelleFenetre.setScene(nouvelleScene);
        nouvelleFenetre.initOwner(Launch.getPrimaryStage());

        Stage stage = (Stage) btnRetour.getScene().getWindow();
        stage.close();

        nouvelleFenetre.show();
    }

    /**
     * Afficher le message d'alerte
     * @param type type de bouton, ici de confirmation pour être sûr de ce que l'utilisateur veut
     * @param header = null
     * @param message Message que l'on veut afficher à l'utilisateur, ici une question
     * @param buttonsAlert les différents boutons utilisé
     * @return
     */
    private Optional<ButtonType> showMessage(Alert.AlertType type, String header, String message, ButtonType... buttonsAlert) {
        Alert fenetreAlerte = new Alert(type);
        fenetreAlerte.setHeaderText(header);
        fenetreAlerte.setContentText(message);
        if (buttonsAlert.length > 0) {
            fenetreAlerte.getButtonTypes().clear();
            fenetreAlerte.getButtonTypes().addAll(buttonsAlert);
        }
        return fenetreAlerte.showAndWait();
    }


    @FXML
    private void toBookMenu() throws IOException{
        Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/BookMenuAnglais.fxml")));
        Stage nouvelleFenetre = new Stage();
        nouvelleFenetre.setScene(nouvelleScene);
        nouvelleFenetre.initOwner(Launch.getPrimaryStage());

        /*
        J'enlève la fermeture de la fenêtre pour que la game reste ouverte en arrière plan

        Stage stage = (Stage) bookRecette.getScene().getWindow();
        stage.close();*/

        nouvelleFenetre.show();
    }

}
