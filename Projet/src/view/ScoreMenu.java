package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.entity.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Vue du menu des scores
 */
public class ScoreMenu {


    @FXML private Button closeButton;
    @FXML private ListView<String> scoreListView;


    /**
     * Amène au menu principal
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {
        if (Launch.isFr){
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            //Ferme la fenêtre courrante
            Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
        else {
            Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            Stage nouvelleFenetre = new Stage();
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();

            nouvelleFenetre.show();
        }
    }


    /**
     * Initialise le tableau des scores en lisant la sauvegarde
     * @throws IOException
     */
    @FXML
    public void initialize() throws IOException {
        List<Joueur> j = new ArrayList<>();
        Joueur joueur;

        BufferedReader fluxEntree= new BufferedReader(new FileReader("src/save/joueur.txt"));
        String ligneLue;



            do {
                ligneLue = fluxEntree.readLine();
                scoreListView.getItems().add(ligneLue);
            }
            while(ligneLue!=null);

            fluxEntree.close();





    }
}

