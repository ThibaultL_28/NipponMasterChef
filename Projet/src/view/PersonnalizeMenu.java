package view;

import launcher.Launch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Vue de personnalisation du jeu
 */
public class PersonnalizeMenu implements Initializable {


    @FXML public ComboBox<Integer> comboboxClient;

    public TextField temps;

    ObservableList<Integer> listClient = FXCollections.observableArrayList(1,2,3);


    @FXML private Button btnRetour;
    @FXML private Button btnJouer;

    @FXML private CheckBox cbEbiFry;
    @FXML private CheckBox cbSashimi;
    @FXML private CheckBox cbNigiris;
    @FXML private CheckBox cbOnigiri;
    @FXML private CheckBox cbSoda;
    @FXML private CheckBox cbEau;
    @FXML private CheckBox cbSake;


    /**
     * Initialise des valeurs par défaut
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        comboboxClient.setItems(listClient);
        comboboxClient.setValue(3);
        temps.setText("120");
        // force the field to be numeric only
        temps.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    temps.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

    }


    /**
     * Va venir prendre en compte les choix de l'utilisateur
     * et va lancer le jeu
     * @throws IOException
     */
    @FXML
    private void toGame() throws IOException {

        if (temps.getText().equals("")){
            Launch.temps = 120;
        }else {
            Launch.temps = Integer.parseInt(temps.getText());
        }

        Launch.nbClient = comboboxClient.getValue();

        if (!cbEau.isSelected()){
            Launch.eau = false;
        }
        if (!cbSake.isSelected()){
            Launch.sake   = false;
        }
        if (!cbSoda.isSelected()){
            Launch.soda  = false;
        }

        //Si le joueur désactive toutes les boissons on désactive les boissons dans le launch
        if (!cbEau.isSelected() && !cbSake.isSelected() && !cbSoda.isSelected()){
            System.out.println(1);
            Launch.boissonActive = false;
        }else if (cbEau.isSelected() || cbSake.isSelected() || cbSoda.isSelected()){
            System.out.println(2);
            Launch.boissonActive = true;
        }

        if (!cbNigiris.isSelected()){
            Launch.nigiri   = false;
        }
        if (!cbOnigiri.isSelected()){
            Launch.onigiri   = false;
        }
        if (!cbSashimi.isSelected()){
            Launch.sashimi  = false;
        }
        if (!cbEbiFry.isSelected()){
            Launch.ebifry = false;
        }

        if (!cbNigiris.isSelected() && !cbOnigiri.isSelected() && !cbSashimi.isSelected() && !cbEbiFry.isSelected()){
            Launch.nigiri = true;
            Launch.onigiri = true;
            Launch.sashimi = true;
            Launch.ebifry = true;
        }

        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if (Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/Game.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnJouer.getScene().getWindow();
            stage.close();

        }
        else {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/GameAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnJouer.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();

    }

    /**
     * Amène a la vue de la selection du niveau
     * @throws IOException
     */
    @FXML
    private void toRetour() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if (Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/LvlMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnRetour.getScene().getWindow();
            stage.close();

        }
        else {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/LvlMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) btnRetour.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();

    }


}
