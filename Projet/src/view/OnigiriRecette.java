package view;

import launcher.Launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Vue recette onigiri
 */
public class OnigiriRecette {

    @FXML private Button Retour;
    @FXML private Button MenuPrincipal;


    /**
     * Amène au menu principal
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if(Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) MenuPrincipal.getScene().getWindow();
            stage.close();

        }
        else {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/MainMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) MenuPrincipal.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();
    }

    /**
     * Amène au livre des recettes
     * @throws IOException
     */
    @FXML
    private void toBookMenu() throws IOException {
        Scene nouvelleScene;
        Stage nouvelleFenetre = new Stage();
        if (Launch.isFr){
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/BookMenu.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Retour.getScene().getWindow();
            stage.close();

        }
        else {
            nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/english/BookMenuAnglais.fxml")));
            nouvelleFenetre.setScene(nouvelleScene);
            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

            Stage stage = (Stage) Retour.getScene().getWindow();
            stage.close();

        }
        nouvelleFenetre.show();

    }



}
