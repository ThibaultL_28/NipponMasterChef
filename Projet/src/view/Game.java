package view;

import launcher.Launch;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.entity.*;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Vue de la partie
 */
public class Game implements Initializable {


    /*
    Variables FXML
     */

    // AnchorPane
    public AnchorPane ap;

    //Label
    @FXML
    public Label compteur;
    @FXML
    public Label cuiseurRiz;
    @FXML
    public Label couteau;
    @FXML
    public Label friteuse;
    @FXML
    public Label argent;

    //ListView
    @FXML
    public ListView assemblage;
    @FXML
    public ListView boisson;

    //Image View
    @FXML
    public ImageView ingredientSelection;
    @FXML
    public ImageView recette1;
    @FXML
    public ImageView recette2;
    @FXML
    public ImageView recette3;
    @FXML
    public ImageView ingredientSelection1;
    @FXML
    public ImageView ingredientSelection2;
    @FXML
    public ImageView perso1;
    @FXML
    public ImageView perso2;
    @FXML
    public ImageView perso3;
    @FXML
    public ImageView boisson1;
    @FXML
    public ImageView boisson2;
    @FXML
    public ImageView boisson3;
    @FXML
    public ImageView soda;
    @FXML
    public ImageView sake;
    @FXML
    public ImageView eau;
    @FXML
    public ImageView imgCuiseur;
    @FXML
    public ImageView imgCouteau;
    @FXML
    public ImageView imgFriteuse;

    //Button
    @FXML
    public Button decoupe;
    @FXML
    public Button decoupe1;
    @FXML
    public Button decoupe2;
    @FXML
    public Button assemble;
    @FXML
    public Button assemble1;
    @FXML
    public Button assemble2;
    @FXML
    public Button cuire;
    @FXML
    public Button cuire1;
    @FXML
    public Button cuire2;
    @FXML
    public Button laverCuiseur;
    @FXML
    public Button laverCouteau;
    @FXML
    public Button frire;
    @FXML
    public Button laverFriteuse;
    @FXML
    public Button frire1;
    @FXML
    public Button frire2;
    @FXML
    private Button btnRetour;
    @FXML
    private Button bookRecette;
    //Ellipse
    @FXML
    public Ellipse bulle1;
    @FXML
    public Ellipse bulle2;
    @FXML
    public Ellipse bulle3;
    @FXML
    public ImageView imgServir;
    @FXML
    public ImageView imgAssiete;

    public ProgressIndicator progressBar;
    public ProgressIndicator progressBar1;
    public ProgressIndicator progressBar2;


    /*
    Autres variables
     */

    public Livre<Recette> livreRecettes;
    public Livre<Ingredient> livreIngredients;
    public Livre<Boisson> livreBoisson;
    public Livre<Materiel> livreMateriel;

    public ArrayList<Ingredient> listAssemblage = new ArrayList<>();
    public ArrayList<Boisson> listAssemblageBoisson = new ArrayList<>();

    public ArrayList<Recette> recettesEnAttente = new ArrayList<>();
    public ArrayList<Boisson> boissonsEnAttente = new ArrayList<>();

    public Ingredient[] listSelection = new Ingredient[3];
    public Boisson[] listSelectionBoisson = new Boisson[3];

    public Recette recetteEnCours;
    public Boisson boissonEnCours;

    public PrepareRecette prepareRecette;

    public int temps = Launch.temps;
    public int tempsLavageCuiseur;
    public int tempsLavageCouteau;
    public int tempsLavageFriteuse;
    public int tempsCuissonRiz;
    public int tempsDecoupe;
    public int tempsFriture;
    public boolean enPause = false;
    public boolean enCuissonRiz = false;
    public boolean enDecoupe = false;
    public boolean enFriture = false;
    public Joueur joueur;


    /**
     * Va venir initialiser les variable du jeu avant le chargement de la vue.
     * En fonction du niveau ou des paramètres choisis dans le menu personalisé.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // initialise le compteur
        compteur.setText("compteur : " + temps);
        tempsNiveau();

        // récupération des recettes, ingrédients, matériel et joueur
        livreRecettes = new Livre<>("recette");
        livreIngredients = new Livre<>("ingredient");
        livreMateriel = new Livre<>("materiel");
        joueur = new Joueur(Launch.nomJoueur, 0);

        //Pour le niveau personalisé choix des recettes
        if (!Launch.sashimi){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("sashimi"));
        }if (!Launch.onigiri){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("onigiri"));
        }if (!Launch.ebifry){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("ebifry"));
        }if (!Launch.nigiri){
            livreRecettes.getLivre().removeIf(r -> r.getNom().equals("nigiri"));
        }

        //Pour le niveau personalisé nb clients
        if (Launch.nbClient == 3){
            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));
            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));
            recette2.setImage(new Image(recettesEnAttente.get(0).getRecettePath()));
            recette3.setImage(new Image(recettesEnAttente.get(1).getRecettePath()));
        } else if (Launch.nbClient == 2){
            perso3.setVisible(false);
            bulle3.setVisible(false);

            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            recettesEnAttente.add(getRandomRecette(livreRecettes.getLivre()));

            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));
            recette2.setImage(new Image(recettesEnAttente.get(0).getRecettePath()));

        } else if (Launch.nbClient == 1){
            perso3.setVisible(false);
            bulle3.setVisible(false);
            perso2.setVisible(false);
            bulle2.setVisible(false);

            recetteEnCours = getRandomRecette(livreRecettes.getLivre());
            String pathRecette1 = recetteEnCours.getRecettePath();
            recette1.setImage(new Image(pathRecette1));

        }


        if (Launch.boissonActive) {

            //activation des boissons
            soda.setDisable(false);
            eau.setDisable(false);
            sake.setDisable(false);
            livreBoisson = new Livre<>("boisson");

            //Pour le niveau personalisé
            if (!Launch.soda){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("soda"));
            }if (!Launch.sake){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("sake"));
            }if (!Launch.eau){
                livreBoisson.getLivre().removeIf(r -> r.getNom().equals("eau"));
            }

            //Pour le niveau personalisé nb clients
            if (Launch.nbClient == 3){
                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));
                boisson2.setImage(new Image(boissonsEnAttente.get(0).getBoissonPath()));
                boisson3.setImage(new Image(boissonsEnAttente.get(1).getBoissonPath()));
            } else if (Launch.nbClient == 2){

                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());
                boissonsEnAttente.add(getRandomBoisson(livreBoisson.getLivre()));

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));
                boisson2.setImage(new Image(boissonsEnAttente.get(0).getBoissonPath()));

            } else if (Launch.nbClient == 1){

                boissonEnCours = getRandomBoisson(livreBoisson.getLivre());

                boisson1.setImage(new Image(boissonEnCours.getBoissonPath()));

            }



        }

        prepareRecette = new PrepareRecette(recetteEnCours);


        tempsLavageCuiseur = getMaterielByName("cuiseurRiz").getTempsNettoyage();
        tempsLavageCouteau = getMaterielByName("couteau").getTempsNettoyage();
        tempsLavageFriteuse = getMaterielByName("friteuse").getTempsNettoyage();
        tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();
        tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();
        tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

        argent.setText("Argent : " + joueur.getArgent());




        /* Exemple animation pour les personnages
        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(2), perso1);
        translateAnimation.setByX(-50);
        translateAnimation.setByY(50);
        translateAnimation.play();

         */


    }

    /**
     * Récupère une recette de manière aléatoire
     * @param list
     * @return
     */
    public Recette getRandomRecette(ArrayList<Recette> list) {
        Random r = new Random();
        int randomNumber = r.nextInt(list.size());
        return list.get(randomNumber);
    }

    /**
     * Récupère une boisson de manière aléatoire
     * @param list
     * @return
     */
    public Boisson getRandomBoisson(ArrayList<Boisson> list) {
        Random r = new Random();
        int randomNumber = r.nextInt(list.size());
        return list.get(randomNumber);
    }

    /**
     * Méthode s'occupant du temps disponible pour finir le niveau
     * Utilise un thread
     */
    public void tempsNiveau() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        temps--;
                        compteur.setText("compteur : " + temps);
                        if (temps == 0) {

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setContentText("Perdu retour au menu principal");
                            alert.showAndWait();

                            Scene nouvelleScene = null;
                            try {
                                nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Stage nouvelleFenetre = new Stage();
                            nouvelleFenetre.setScene(nouvelleScene);
                            nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                            Stage stage = (Stage) ap.getScene().getWindow();
                            stage.close();

                            nouvelleFenetre.show();
                        }
                    }
                };
                while (!enPause) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Selectionne un ingrédient et le place d'en un des 3 espaces de travail
     * @param mouseEvent
     */
    public void selectionIngredient(MouseEvent mouseEvent) {

        ImageView imageView = (ImageView) mouseEvent.getSource();
        Image image = imageView.getImage();

        // place l'ingredient dans le premier enplacement si il n'est pas null dans le deuxième sinon
        // et enfin le troisième. Si les 3 soit pris ne fait rien
        if (ingredientSelection.getImage() == null) {
            ingredientSelection.setImage(image);
            ingredientSelection.setAccessibleText(imageView.getAccessibleText());

            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[0] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }

            // Si l'item selectionné n'est pas un ingrédient il s'agit donc d'une boisson
            // Et place la boissons dans une liste
            if (listSelection[0] == null) {
                listSelection[0] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[0] = getBoissonByName(imageView.getAccessibleText());
            }

        } else if (ingredientSelection1.getImage() == null) {
            ingredientSelection1.setImage(image);
            ingredientSelection1.setAccessibleText(imageView.getAccessibleText());
            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[1] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }
            //listSelection[1] = (getIngredientByName(imageView.getAccessibleText()));
            if (listSelection[1] == null) {
                listSelection[1] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[1] = getBoissonByName(imageView.getAccessibleText());
            }
        } else if (ingredientSelection2.getImage() == null) {
            ingredientSelection2.setImage(image);
            ingredientSelection2.setAccessibleText(imageView.getAccessibleText());
            Ingredient ingredient = getIngredientByName(imageView.getAccessibleText());
            if (ingredient != null) {
                listSelection[2] = new Ingredient(ingredient.getNom(), ingredient.getTempsCuisson(), ingredient.isEstCuit(), ingredient.isEstCoupe());
            }
            //
            listSelection[2] = (getIngredientByName(imageView.getAccessibleText()));
            if (listSelection[2] == null) {
                listSelection[2] = new Ingredient("null", 0, true, true);
                listSelectionBoisson[2] = getBoissonByName(imageView.getAccessibleText());
            }
        }
        //Pour récupérer le nom des ingrédients
        System.out.println(imageView.getAccessibleText());

    }

    /**
     * Va venir retirer l'aliment de la liste d'assemblage
     * @param contextMenuEvent
     */
    public void desassembler(MouseEvent contextMenuEvent) {
        int index = assemblage.getSelectionModel().getSelectedIndex();
        assemblage.getItems().remove(index);
        listAssemblage.remove(index);
    }

    /**
     * Va venir retirer la boisson de la liste d'assemblage
     * @param mouseEvent
     */
    public void desassemblerBoisson(MouseEvent mouseEvent) {
        int index = boisson.getSelectionModel().getSelectedIndex();
        boisson.getItems().remove(index);
        listAssemblageBoisson.remove(index);
    }


    /**
     * Va venir assembler les aliments/boissons
     * @param actionEvent
     */
    public void assembler(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();

        // Si il s'agit du premier emplacement d'assemblage retire l'aliment ou boisson
        // et le place dans la liste d'assemblage même principe pour les autres emplacement
        if (b.getId().equals("assemble")) {
            if (ingredientSelection.getImage() != null) {
                if (!listSelection[0].getNom().equals("null")) {
                    progressBar.setProgress(0);
                    assemblage.getItems().add(ingredientSelection.getAccessibleText());
                    listAssemblage.add(listSelection[0]);
                    listSelection[0] = null;
                    ingredientSelection.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[0]);
                    listSelectionBoisson[0] = null;
                    ingredientSelection.setImage(null);
                }
            }

        } else if (b.getId().equals("assemble1")) {
            if (ingredientSelection1.getImage() != null) {
                if (!listSelection[1].getNom().equals("null")) {
                    progressBar1.setProgress(0);
                    assemblage.getItems().add(ingredientSelection1.getAccessibleText());
                    listAssemblage.add(listSelection[1]);
                    listSelection[1] = null;
                    ingredientSelection1.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection1.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[1]);
                    listSelectionBoisson[1] = null;
                    ingredientSelection1.setImage(null);
                }
            }
        } else {

            if (ingredientSelection2.getImage() != null) {

                if (!listSelection[2].getNom().equals("null")) {
                    progressBar2.setProgress(0);
                    assemblage.getItems().add(ingredientSelection2.getAccessibleText());
                    listAssemblage.add(listSelection[2]);
                    listSelection[2] = null;
                    ingredientSelection2.setImage(null);
                } else {
                    boisson.getItems().add(ingredientSelection2.getAccessibleText());
                    listAssemblageBoisson.add(listSelectionBoisson[2]);
                    listSelectionBoisson[2] = null;
                    ingredientSelection2.setImage(null);
                }
            }
        }
    }

    /**
     * Va venir activer la découpe (ici il n'y aura que le saumon)
     * Va lancer un thread pour na pas bloquer le jeu
     * @param actionEvent
     */
    public void decoupe(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();

        // Si on est sur le premier emplacement
        if (b.getId().equals("decoupe")) {
            // on vérifie que ça ne soit pas null et qu'il s'agisse bien d'un aliment que l'on peut couper
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {

                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble.setDisable(true);

                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    // on lance la découpe, même principe pour les autres emplacements et les actions
                    // frire, cuire riz...
                    // Le tread est lancé via la méthode en dessous
                    decoupeSaumon(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop découpe");

                } else {

                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble.setDisable(false);


                    enDecoupe = false;
                    b.setText("Découpe");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }

                //ingredientSelection.setAccessibleText("saumon découpé");
            }
        } else if (b.getId().equals("decoupe1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {
                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble1.setDisable(true);


                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    decoupeSaumon(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop découpe");



                } else {
                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble1.setDisable(false);

                    enDecoupe = false;
                    b.setText("Découpe");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }

            }
        } else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("saumon")) {
                if (getMaterielByName("couteau").isPropre() && !enDecoupe) {
                    decoupe.setDisable(true);
                    decoupe1.setDisable(true);
                    decoupe2.setDisable(true);
                    assemble2.setDisable(true);

                    enDecoupe = true;
                    getMaterielByName("couteau").setPropre(false);
                    decoupeSaumon(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop découpe");


                } else {
                    decoupe.setDisable(false);
                    decoupe1.setDisable(false);
                    decoupe2.setDisable(false);
                    assemble2.setDisable(false);

                    enDecoupe = false;
                    b.setText("Découpe");
                    tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();

                }

            }
        }
    }

    /**
     * Va venir activé la friture même principe que la méthode découpe
     * Va lancer un thread pour na pas bloquer le jeu
     * @param actionEvent
     */
    public void frire(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getSource();
        if (b.getId().equals("frire")) {
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop friture");

                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble.setDisable(false);

                    enFriture = false;
                    b.setText("Frire");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }else if (b.getId().equals("frire1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble1.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop friture");


                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble1.setDisable(false);

                    enFriture = false;
                    b.setText("Frire");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("ebifry")) {
                if (getMaterielByName("friteuse").isPropre() && !enFriture) {

                    frire.setDisable(true);
                    frire1.setDisable(true);
                    frire2.setDisable(true);
                    assemble2.setDisable(true);

                    enFriture = true;
                    getMaterielByName("friteuse").setPropre(false);
                    frireEbi(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop friture");

                    /*
                    ingredientSelection.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                    listSelection[0].setEstCuit(true);
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuiseurRiz.setText("Sale");
                    laverCuiseur.setDisable(false);
                     */
                    //ingredientSelection.setAccessibleText("riz cuit");
                } else {
                    frire.setDisable(false);
                    frire1.setDisable(false);
                    frire2.setDisable(false);
                    assemble2.setDisable(false);

                    enFriture = false;
                    b.setText("Frire");
                    tempsFriture = getIngredientByName("ebifry").getTempsCuisson();

                }
            }
        }
    }


    /**
     * Va venir activé la cuisson même principe que la méthode découpe
     * Va lancer un thread pour na pas bloquer le jeu
     * @param actionEvent
     */
    public void cuire(ActionEvent actionEvent) {

        Button b = (Button) actionEvent.getSource();
        if (b.getId().equals("cuire")) {
            // temps de cuissson a faire
            if (ingredientSelection.getImage() != null && ingredientSelection.getAccessibleText().equals("riz")) {
                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {

                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble.setDisable(true);


                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection, listSelection[0], 1);
                    b.setText("Stop cuisson");

                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cuire");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }
            }
        } else if (b.getId().equals("cuire1")) {
            if (ingredientSelection1.getImage() != null && ingredientSelection1.getAccessibleText().equals("riz")) {
                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {
                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble1.setDisable(true);

                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection1, listSelection[1], 2);
                    b.setText("Stop cuisson");

                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble1.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cuire");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }

            }
        } else {
            if (ingredientSelection2.getImage() != null && ingredientSelection2.getAccessibleText().equals("riz")) {

                if (getMaterielByName("cuiseurRiz").isPropre() && !enCuissonRiz) {
                    cuire.setDisable(true);
                    cuire1.setDisable(true);
                    cuire2.setDisable(true);
                    assemble2.setDisable(true);

                    enCuissonRiz = true;
                    getMaterielByName("cuiseurRiz").setPropre(false);
                    cuireRiz(actionEvent, ingredientSelection2, listSelection[2], 3);
                    b.setText("Stop cuisson");

                } else {
                    cuire.setDisable(false);
                    cuire1.setDisable(false);
                    cuire2.setDisable(false);
                    assemble2.setDisable(false);

                    enCuissonRiz = false;
                    b.setText("Cuire");
                    tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();

                }


            }
        }

    }

    /**
     * Récupère un ingrédient en fonction de son nom
     * @param nom
     * @return
     */
    public Ingredient getIngredientByName(String nom) {
        for (Ingredient t : livreIngredients.getLivre()) {
            if (t.getNom().equals(nom)) {
                return t;
            }
        }
        return null;
    }

    /**
     * Récupère un matériel en fonction de son nom
     * @param nom
     * @return
     */
    public Materiel getMaterielByName(String nom) {
        for (Materiel m : livreMateriel.getLivre()) {
            if (m.getNom().equals(nom)) {
                return m;
            }
        }
        return null;
    }

    /**
     * récupère une boisson en fonction de son nom
     * @param nom
     * @return
     */
    public Boisson getBoissonByName(String nom) {
        for (Boisson b : livreBoisson.getLivre()) {
            if (b.getNom().equals(nom)) {
                return b;
            }
        }
        return null;
    }

    /**
     * Méthode qui va venir servir le client
     * Va vérifier qu'il s'agisse bien de la bonne recette
     * Va vérifier la fin de partie
     * @param actionEvent
     * @throws IOException
     */
    public void servir(ActionEvent actionEvent) throws IOException {

        boolean pret = true;

        prepareRecette.setOrdreAssemblageEffectue(listAssemblage);

        // Prend en compte les boissons si elles sont activé
        if (Launch.boissonActive) {
            pret = false;
            if ((!boisson.getItems().isEmpty()) && boissonEnCours.equals(listAssemblageBoisson.get(0)) && boisson.getItems().size() == 1) {
                pret = true;
            }
        }

        // si la recette est prête
        if (prepareRecette.estPrete() && pret) {

            // Si le premier client n'est pas servi
            if (recette1.isVisible()) {

                // si les boissons sont activées
                if (Launch.boissonActive) {
                    boisson1.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {

                        boissonEnCours = boissonsEnAttente.remove(0);

                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette1.setVisible(false);
                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());
                argent.setText("Argent : " + joueur.getArgent());


                // animation du service
                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(-500);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(-500);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle1.setVisible(false);
                    perso1.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(500);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(500);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));

                // si il reste des recettes en attente on passe a la suivante
                if (!recettesEnAttente.isEmpty()) {



                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);

                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();

                }
                // sinon toutes les recettes on été faites le joueur a gagné et on enregistre son score
                else {

                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", argent : " + joueur.getArgent() + "\n");
                    pw.close();



                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Niveau terminé argent gagné : " + joueur.getArgent());
                    enPause = true;
                    alert.showAndWait();

                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
               // Même principe pour les autres recettes si il y en a
                // En effet via la personnalisation on peut choisir qu'un seul
                // client si on en a l'envie
            } else if (recette2.isVisible()) {
                if (Launch.boissonActive) {
                    boisson2.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {
                        boissonEnCours = boissonsEnAttente.remove(0);
                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette2.setVisible(false);
                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());
                argent.setText("Argent : " + joueur.getArgent());


                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(-150);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(-150);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle2.setVisible(false);
                    perso2.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(150);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(150);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));


                if (!recettesEnAttente.isEmpty()) {
                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);
                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();


                } else {

                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", argent : " + joueur.getArgent() + "\n");
                    pw.close();


                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Niveau terminé argent gagné : " + joueur.getArgent());
                    enPause = true;
                    alert.showAndWait();

                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
            } else if (recette3.isVisible()) {
                if (Launch.boissonActive) {
                    boisson3.setVisible(false);
                    joueur.setArgent(joueur.getArgent() + boissonEnCours.getArgentBoisson());
                    if (!recettesEnAttente.isEmpty()) {
                        boissonEnCours = boissonsEnAttente.remove(0);
                    }
                    listAssemblageBoisson = new ArrayList<>();
                    boisson.getItems().clear();
                }
                recette3.setVisible(false);

                joueur.setArgent(joueur.getArgent() + recetteEnCours.getArgentRecette());
                argent.setText("Argent : " + joueur.getArgent());


                imgAssiete.toFront();
                imgServir.toFront();
                final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgAssiete);
                final TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(3), imgServir);
                final TranslateTransition translateAnimation3 = new TranslateTransition(Duration.seconds(3), imgServir);
                translateAnimation.setByX(200);
                translateAnimation.setByY(-300);
                translateAnimation2.setByX(200);
                translateAnimation2.setByY(-300);
                translateAnimation.play();
                translateAnimation2.play();

                translateAnimation2.setOnFinished((ActionEvent event) -> {
                    bulle3.setVisible(false);
                    perso3.setVisible(false);
                    imgServir.setImage(null);
                    translateAnimation.setByX(-200);
                    translateAnimation.setByY(300);
                    translateAnimation3.setByX(-200);
                    translateAnimation3.setByY(300);
                    translateAnimation.play();
                    translateAnimation3.play();

                });

                imgServir.setImage(new Image(recetteEnCours.getRecettePath()));


                if (!recettesEnAttente.isEmpty()) {
                    recetteEnCours = recettesEnAttente.remove(0);
                    prepareRecette = new PrepareRecette(recetteEnCours);
                    listAssemblage = new ArrayList<>();
                    assemblage.getItems().clear();


                } else {


                    PrintWriter pw = new PrintWriter(new FileOutputStream(new File("src/save/joueur.txt"), true /* append = true */));
                    pw.write(joueur.getNom() + ", argent : " + joueur.getArgent() + "\n");
                    pw.close();

                    Launch.niveau1Termine = true;

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Niveau terminé argent gagné : " + joueur.getArgent());
                    enPause = true;
                    alert.showAndWait();
                    Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
                    Stage nouvelleFenetre = new Stage();
                    nouvelleFenetre.setScene(nouvelleScene);
                    nouvelleFenetre.initOwner(Launch.getPrimaryStage());

                    Stage stage = (Stage) ap.getScene().getWindow();
                    stage.close();

                    nouvelleFenetre.show();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Mauvaise recette");
            alert.showAndWait();
        }
    }

    /**
     * Lance le thread pour respecter le temps de lavage du cuiseur a riz
     * @param actionEvent
     */
    public void laverCuiseur(ActionEvent actionEvent) {

        laverCuiseur.setDisable(true);
        laverCuiseur.setText("En lavage");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCuiseur);
        translateAnimation.setByX(-200);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageCuiseur--;
                        cuiseurRiz.setText("temps : " + tempsLavageCuiseur);
                        if (tempsLavageCuiseur < 0) {
                            cuiseurRiz.setText("propre");
                            getMaterielByName("cuiseurRiz").setPropre(true);
                            tempsLavageCuiseur = getMaterielByName("cuiseurRiz").getTempsNettoyage();
                            //laverCuiseur.setDisable(false);
                            laverCuiseur.setText("laverCuiseur");
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageCuiseur > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Lance un thread pour respecter le temps de lavage du couteau
     * @param actionEvent
     */
    public void laverCouteau(ActionEvent actionEvent) {

        laverCouteau.setDisable(true);
        laverCouteau.setText("En lavage");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgCouteau);
        translateAnimation.setByX(-300);
        translateAnimation.setByY(-80);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageCouteau--;
                        couteau.setText("temps : " + tempsLavageCouteau);
                        if (tempsLavageCouteau < 0) {
                            couteau.setText("propre");
                            getMaterielByName("couteau").setPropre(true);
                            tempsLavageCouteau = getMaterielByName("couteau").getTempsNettoyage();
                            // laverCouteau.setDisable(false);
                            laverCouteau.setText("laverCouteau");
                            translateAnimation.setByX(300);
                            translateAnimation.setByY(80);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageCouteau > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Lance un thread pour respecter le temps de lavage de la friteuse
     * @param actionEvent
     */
    public void laverFriteuse(ActionEvent actionEvent) {

        laverFriteuse.setDisable(true);
        laverFriteuse.setText("En lavage");

        final TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(3), imgFriteuse);
        translateAnimation.setByX(-300);
        translateAnimation.setByY(-120);
        translateAnimation.play();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsLavageFriteuse--;
                        friteuse.setText("temps : " + tempsLavageFriteuse);
                        if (tempsLavageFriteuse < 0) {
                            friteuse.setText("propre");
                            getMaterielByName("friteuse").setPropre(true);
                            tempsLavageFriteuse = getMaterielByName("friteuse").getTempsNettoyage();
                            // laverCouteau.setDisable(false);
                            laverFriteuse.setText("laverFriteuse");
                            translateAnimation.setByX(300);
                            translateAnimation.setByY(120);
                            translateAnimation.play();
                        }
                    }
                };
                while (tempsLavageFriteuse > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Lance un tread pour lancer la cuissson du riz
     * @param actionEvent
     * @param ingredientSelectnf image de l'ingrédient sélectionné
     * @param listSelectnf ingrédient séléctionné
     * @param nbProgressBar numéro de la progress bar-
     */
    public void cuireRiz(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsCuissonRiz;

        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("En cuisson");


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsCuissonRiz--;

                        if (tempsCuissonRiz < 0 && tempsCuissonRiz > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/rice2.png"));
                            listSelect[0].setEstCuit(true);
                            cuiseurRiz.setText("Sale");
                            laverCuiseur.setDisable(false);
                            b.setDisable(false);

                        } else if (tempsCuissonRiz < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }
                            cuiseurRiz.setText("Brulé");
                            // application du malus
                            tempsLavageCuiseur = tempsLavageCuiseur + 5;
                            enCuissonRiz = false;

                            cuire.setDisable(false);
                            cuire1.setDisable(false);
                            cuire2.setDisable(false);

                            b.setText("Cuire");
                            tempsCuissonRiz = getIngredientByName("riz").getTempsCuisson();
                        }
                    }
                };
                while (enCuissonRiz) {
                    if (nbProgressBar == 1){
                        progressBar.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }else if (nbProgressBar == 2){
                        progressBar1.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }else if (nbProgressBar == 3){
                        progressBar2.setProgress(((timeProgressBar - tempsCuissonRiz)/timeProgressBar));
                    }
                    try {
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Lance un thread pour lancer la découpe du saumon
     * @param actionEvent
     * @param ingredientSelectnf image de l'ingrédient séléctionné
     * @param listSelectnf ingrédient sélécionné
     * @param nbProgressBar numéro de la progress bar
     */
    public void decoupeSaumon(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsDecoupe;

        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("En découpe");



        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsDecoupe--;
                        // cuiseurRiz.setText("temps : " + tempsLavageCuiseur);
                        if (tempsDecoupe < 0 && tempsDecoupe > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/morceauFish2.png"));
                            listSelect[0].setEstCoupe(true);
                            couteau.setText("Sale");
                            laverCouteau.setDisable(false);
                            b.setDisable(false);

                        } else if (tempsDecoupe < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }

                            couteau.setText("Très sale");
                            // application du malus
                            tempsLavageCouteau = tempsLavageCouteau + 5;
                            enDecoupe = false;

                            decoupe.setDisable(false);
                            decoupe1.setDisable(false);
                            decoupe2.setDisable(false);

                            b.setText("Découpe");
                            tempsDecoupe = getIngredientByName("saumon").getTempsCuisson();
                        }
                    }
                };
                while (enDecoupe) {
                    try {
                        if (nbProgressBar == 1){
                            progressBar.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }else if (nbProgressBar == 2){
                            progressBar1.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }else if (nbProgressBar == 3){
                            progressBar2.setProgress(((timeProgressBar - tempsDecoupe)/timeProgressBar));
                        }
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Lance un thread pour lancer la friture
     * @param actionEvent
     * @param ingredientSelectnf image ingrédient selectionné
     * @param listSelectnf ingrédient séléctionné
     * @param nbProgressBar le numéro de la progress bar
     */
    public void frireEbi(ActionEvent actionEvent, ImageView ingredientSelectnf, Ingredient listSelectnf, int nbProgressBar) {

        final ImageView ingredientSelect = ingredientSelectnf;
        final Ingredient[] listSelect = {listSelectnf};
        double timeProgressBar = tempsFriture;


        Button b = (Button) actionEvent.getSource();

        b.setDisable(true);
        b.setText("En friture");



        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                        tempsFriture--;
                        if (tempsFriture < 0 && tempsFriture > -10) {
                            ingredientSelect.setImage(new Image("file:ressources/fxml/images/Ebifry2.jpg"));
                            listSelect[0].setEstCuit(true);
                            friteuse.setText("Sale");
                            laverFriteuse.setDisable(false);
                            b.setDisable(false);
                            /*
                            translateAnimation.setByX(200);
                            translateAnimation.play();
                             */
                        } else if (tempsFriture < -10) {
                            ingredientSelect.setImage(null);
                            listSelect[0] = null;

                            if (nbProgressBar == 1){
                                progressBar.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 2){
                                progressBar1.setStyle("-fx-accent: red;");
                            } else if (nbProgressBar == 3){
                                progressBar2.setStyle("-fx-accent: red;");
                            }
                            friteuse.setText("Brulé");
                            // application du malus
                            tempsLavageFriteuse = tempsLavageFriteuse + 5;
                            enFriture = false;

                            frire.setDisable(false);


                            b.setText("Frire");
                            tempsFriture = getIngredientByName("ebifry").getTempsCuisson();
                        }
                    }
                };
                while (enFriture) {
                    if (nbProgressBar == 1){
                        progressBar.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }else if (nbProgressBar == 2){
                        progressBar1.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }else if (nbProgressBar == 3){
                        progressBar2.setProgress(((timeProgressBar - tempsFriture)/timeProgressBar));
                    }
                    try {
                        System.out.println("boup");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    Platform.runLater(updater);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Si l'utilisateur clique sur cancel, alors il reste sur le jeu
     * si l'utilisateur clique sur YES, alors il retourne directement au menu du jeu
     * @throws IOException
     */
    @FXML
    private void toMainMenu() throws IOException {

        enPause = true;
        showMessage(Alert.AlertType.CONFIRMATION, null, "Êtes-vous sûr de vouloir retouner au Menu Principal?", ButtonType.CANCEL, ButtonType.YES)
                .filter(bouton -> bouton == ButtonType.YES)
                .ifPresentOrElse(
                        (bouton) -> {
                            try {
                                goInMenu();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        },
                        () -> {
                            enPause = false;
                            tempsNiveau();
                        });
    }


    /**
     * Permet d'aller dans le menu après avoir confirmer pour quitter la partie
     * Condition pour le YES dans une autre méthode afin de pouvoir la mettre dans le .ifPresent
     * @throws IOException
     */
    private void goInMenu() throws IOException{
        Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml")));
        Stage nouvelleFenetre = new Stage();
        nouvelleFenetre.setScene(nouvelleScene);
        nouvelleFenetre.initOwner(Launch.getPrimaryStage());

        Stage stage = (Stage) btnRetour.getScene().getWindow();
        stage.close();

        nouvelleFenetre.show();
    }

    /**
     * Afficher le message d'alerte
     * @param type type de bouton, ici de confirmation pour être sûr de ce que l'utilisateur veut
     * @param header = null
     * @param message Message que l'on veut afficher à l'utilisateur, ici une question
     * @param buttonsAlert les différents boutons utilisé
     * @return
     */
    private Optional<ButtonType> showMessage(Alert.AlertType type, String header, String message, ButtonType... buttonsAlert) {
        Alert fenetreAlerte = new Alert(type);
        fenetreAlerte.setHeaderText(header);
        fenetreAlerte.setContentText(message);
        if (buttonsAlert.length > 0) {
            fenetreAlerte.getButtonTypes().clear();
            fenetreAlerte.getButtonTypes().addAll(buttonsAlert);
        }
        return fenetreAlerte.showAndWait();
    }


    @FXML
    private void toBookMenu() throws IOException{
        Scene nouvelleScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/BookMenu.fxml")));
        Stage nouvelleFenetre = new Stage();
        nouvelleFenetre.setScene(nouvelleScene);
        nouvelleFenetre.initOwner(Launch.getPrimaryStage());

        /*
        J'enlève la fermeture de la fenêtre pour que la game reste ouverte en arrière plan

        Stage stage = (Stage) bookRecette.getScene().getWindow();
        stage.close();*/

        nouvelleFenetre.show();
    }

}
