package launcher;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.net.URISyntaxException;

/**
 * Lancement du jeu
 */
public class Launch extends Application {

    private static Stage primaryStage;
    public static int nbClient = 3;
    public static int temps = 120;

    public static boolean boissonActive = false;

    public static boolean nigiri = true;
    public static boolean onigiri = true;
    public static boolean ebifry = true;
    public static boolean sashimi = true;
    public static boolean soda = true;
    public static boolean sake = true;
    public static boolean eau = true;

    public static boolean niveau1Termine = false;

    public static boolean isFr = true;

    public static String nomJoueur = "Inconnu";

    private static Media media;
    public static MediaPlayer mediaPlayer;


    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuGame.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        startMusique();
        stage.show();

    }



    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void setPrimaryStage(Stage primaryStage) {
        Launch.primaryStage = primaryStage;
    }

    public static void startMusique() throws URISyntaxException {

        media = new Media(Launch.class.getResource("/music/musique.mp3").toURI().toString());;
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.play();

    }

    public static void stopMusique(){

        mediaPlayer.setMute(true);

    }

}
