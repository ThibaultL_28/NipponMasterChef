package model.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test de la classe matériel
 * Coverage matériel
 * Class 100%
 * Method 87% (la méthode main n'est pas testé)
 * Line 94% (la méthode main n'est pas testé)
 */
public class MaterielUnitTest {

    @Test
    public void creeAllMaterielsTest(){

        //On crée toutes les meteriels
        ArrayList<Materiel> materiels = Materiel.creeAllMateriels();

        //On vérifie la taille
        Assert.assertEquals(3, materiels.size());

        //On vérifie l'ordre et le nom
        Assert.assertEquals("cuiseurRiz", materiels.get(0).getNom());
        Assert.assertEquals("friteuse", materiels.get(1).getNom());
        Assert.assertEquals("couteau", materiels.get(2).getNom());

    }

    @Test
    public void getterTest(){

        //On crée toutes les meteriels
        ArrayList<Materiel> materiels = Materiel.creeAllMateriels();

        //On prend un matériel
        Materiel materiel = materiels.get(0);

        //On test les getters
        //Le nom
        Assert.assertEquals("cuiseurRiz", materiel.getNom());
        //Propreté
        Assert.assertTrue(materiel.isPropre());
        //Temps lavage
        Assert.assertEquals(10, materiel.getTempsNettoyage());
    }

    @Test
    public void setterTest(){

        //On crée toutes les meteriels
        ArrayList<Materiel> materiels = Materiel.creeAllMateriels();

        //On prend un matériel
        Materiel materiel = materiels.get(0);

        //On set la propreté a false
        materiel.setPropre(false);

        //On vérifie que ça fonctionne
        Assert.assertFalse(materiel.isPropre());
    }

    @Test
    public void toStringTest(){

        //On crée toutes les meteriels
        ArrayList<Materiel> materiels = Materiel.creeAllMateriels();

        //On prend un matériel
        Materiel materiel = materiels.get(0);

        //On test le toString
        Assert.assertEquals("Materiel{nom='cuiseurRiz', " +
                "propre=true, tempsNettoyage=10}", materiel.toString());

    }
}
