package model.entity;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test de la class Joueur
 * Coverage Joueur
 * Class 100%
 * Method 100%
 * Line 100%
 */
public class JoueurUnitTest {

    @Test
    public void toStringTest(){

        //Création d'un joueur
        Joueur joueur = new Joueur("Test", 120);

        //Test de la méthode toString
        Assert.assertEquals("Joueur{nom='Test', argent=120}", joueur.toString());
    }

    @Test
    public void getterTest(){

        //Création d'un joueur
        Joueur joueur = new Joueur("Test", 120);

        //On test les getters
        //Le nom
        Assert.assertEquals("Test", joueur.getNom());
        //L'argent
        Assert.assertEquals(120, joueur.getArgent());


    }

    @Test
    public void setterTest(){

        //Création d'un joueur
        Joueur joueur = new Joueur("Test", 120);

        //On test le setter
        joueur.setArgent(150);

        //L'argent
        Assert.assertEquals(150, joueur.getArgent());


    }
}
