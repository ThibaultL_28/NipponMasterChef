package model.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test de la classe Recette
 * Coverage Recette
 * Class 100%
 * Method 87% (la méthode main n'est pas testé)
 * Line 96% (la méthode main n'est pas testé)
 */
public class RecetteUnitTest {

    @Test
    public void creeAllRecettesTest(){

        //On crée toutes les recettes
        ArrayList<Recette> recettes = Recette.creeAllRecettes();

        //On vérifie la taille
        Assert.assertEquals(4, recettes.size());

        //On vérifie l'ordre et le nom
        Assert.assertEquals("nigiri", recettes.get(0).getNom());
        Assert.assertEquals("onigiri", recettes.get(1).getNom());
        Assert.assertEquals("ebifry", recettes.get(2).getNom());
        Assert.assertEquals("sashimi", recettes.get(3).getNom());

    }

    @Test
    public void getterTest(){

        //On crée toutes les recettes
        ArrayList<Recette> recettes = Recette.creeAllRecettes();

        //On prend une recette pour crée les getters ici ebiFry
        Recette recette = recettes.get(2);

        ArrayList<Ingredient> ordreAssemblageExpected = new ArrayList<>();
        ordreAssemblageExpected.add(new Ingredient("ebifry", 5, true, true));

        //On test les getters
        //Le nom
        Assert.assertEquals("ebifry", recette.getNom());
        //L'assemblage
        Assert.assertEquals(ordreAssemblageExpected, recette.getOrdreAssemblageAttendu());
        //L'argent
        Assert.assertEquals(10, recette.getArgentRecette());
        //Le path
        Assert.assertEquals("file:ressources/fxml/images/Ebifry2.jpg", recette.getRecettePath());
    }

    @Test
    public void toStringTest(){

        //On crée toutes les recettes
        ArrayList<Recette> recettes = Recette.creeAllRecettes();

        //On prend une recette
        Recette recette = recettes.get(2);

        //On test le toString
        Assert.assertEquals("Recette{ingredients=[Ingredient{tempsCuisson=5, estCuit=true, brule=false, " +
                "tempsCuissonEcoule=0, enCuisson=false, continuerCuisson=true, nom='ebifry', estCoupe=true}], nom='ebifry', " +
                "aCuire=[Ingredient{tempsCuisson=5, estCuit=true, brule=false, tempsCuissonEcoule=0, enCuisson=false, continuerCuisson=true, " +
                "nom='ebifry', estCoupe=true}], ordreAssemblageAttendu=[Ingredient{tempsCuisson=5, estCuit=true, brule=false, tempsCuissonEcoule=0, " +
                "enCuisson=false, continuerCuisson=true, nom='ebifry', estCoupe=true}], materiels=null, pointsRecette=10}"
                , recette.toString());

    }

}
