package model.entity;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test de la class Boisson
 * Coverage Boisson
 * Class 100%
 * Method 87% (la méthode main n'est pas testé)
 * Line 95%  (la méthode main n'est pas testé)
 */
public class BoissonUnitTest {

    @Test
    public void creeAllBoissonsTest(){

        //On crée toutes les boissons
        ArrayList<Boisson> boissons = Boisson.creeAllBoissons();

        //On vérifie la taille
        Assert.assertEquals(3, boissons.size());

        //On vérifie l'ordre et le nom
        Assert.assertEquals("soda", boissons.get(0).getNom());
        Assert.assertEquals("sake", boissons.get(1).getNom());
        Assert.assertEquals("eau", boissons.get(2).getNom());

    }

    @Test
    public void equalsTest(){

        //On crée toutes les boissons
        ArrayList<Boisson> boissons = Boisson.creeAllBoissons();

        //On prend une boisson
        Boisson boisson = boissons.get(0);

        //On crée une boisson identique pour le equals que nous avons réécris
        Boisson boisson1 = new Boisson("soda", 10, "file:ressources/fxml/images/soda.png");

        //On test la méthode equals
        Assert.assertTrue(boisson.equals(boisson1));

    }

    @Test
    public void getterTest(){

        //On crée toutes les boissons
        ArrayList<Boisson> boissons = Boisson.creeAllBoissons();

        //On prend une boisson
        Boisson boisson = boissons.get(0);

        //On test les getters
        //Nom
        Assert.assertEquals("soda", boisson.getNom());
        //path
        Assert.assertEquals("file:ressources/fxml/images/soda.png", boisson.getBoissonPath());
        //Argent
        Assert.assertEquals(10, boisson.getArgentBoisson());


    }

    @Test
    public void toStringTest(){

        //On crée toutes les boissons
        ArrayList<Boisson> boissons = Boisson.creeAllBoissons();

        //On prend une boisson
        Boisson boisson = boissons.get(0);

        //On test le toString
        Assert.assertEquals("Boisson{nom='soda', pointBoisson=10}", boisson.toString());

    }
}
