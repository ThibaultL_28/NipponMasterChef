package model.entity;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test de la class Livre
 * Coverage livre
 * Class 100%
 * Method 100%
 * Line 100%
 */
public class LivreUnitTest {

    @Test
    public void recuperationLivreTest(){

        //On test la création des livres a partir de la sauvegarde txt
        Livre<Recette> livreRecettes = new Livre<>("recette");
        Livre<Ingredient> livreIngredients = new Livre<>("ingredient");
        Livre<Materiel> livreMateriel = new Livre<>("materiel");
        Livre<Boisson> livreBoisson = new Livre<>("boisson");

        //On test la récupération du livre et sa taille
        Assert.assertEquals(4, livreRecettes.getLivre().size());
        Assert.assertEquals(4, livreIngredients.getLivre().size());
        Assert.assertEquals(3, livreMateriel.getLivre().size());
        Assert.assertEquals(3, livreBoisson.getLivre().size());

    }

}
