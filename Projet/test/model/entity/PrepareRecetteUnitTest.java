package model.entity;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test de la préparation d'un recette
 * On choisira ici une recette simple afin
 * de simplifier la compréhension du test
 * Coverage PrepareRecette :
 * Class 100%
 * Method 100%
 *
 */
public class PrepareRecetteUnitTest {

    @Test
    public void addIngredientInListAssemblageEffectueTest(){
        //On crée une préparation d'un recette null car nous avons pas besoin d'une preparation de recette pour ce test
        PrepareRecette prepareRecette = new PrepareRecette(null);
        Livre<Ingredient> livreIngredients = new Livre<>("ingredient");
        Ingredient ebifryIngredient = null;

        //List vide que l'on remplira pour la comparer à celle de la classe PrepareRecette
        ArrayList<Ingredient> ingredients = new ArrayList<>();

        //On récupère un ingredient
        for (Ingredient i: livreIngredients.getLivre()) {
            if (i.getNom().equals("ebifry")){
                ebifryIngredient = i;
            }
        }

        //On ajoute l'ebifry l'assemblage
        prepareRecette.addIngredientInListAssemblageEffectue(ebifryIngredient);

        //On ajoute l'ebifry à la liste qui permettra de faire le test
        ingredients.add(ebifryIngredient);

        Assert.assertEquals(ingredients, prepareRecette.getOrdreAssemblageEffectue());

    }

    @Test
    public void setOrdreAssemblageEffectueTest(){
        //On crée une préparation d'un recette null car nous avons pas besoin d'une preparation de recette pour ce test
        PrepareRecette prepareRecette = new PrepareRecette(null);
        Livre<Ingredient> livreIngredients = new Livre<>("ingredient");
        Ingredient ebifryIngredient = null;

        //List qui nous permettra de vérifier la méthode setOrdreAssemblageEffectue
        ArrayList<Ingredient> ingredients = new ArrayList<>();

        //On récupère un ingredient
        for (Ingredient i: livreIngredients.getLivre()) {
            if (i.getNom().equals("ebifry")){
                ebifryIngredient = i;
            }
        }

        //On ajoute l'ebifry à la liste qui permettra de faire le test
        ingredients.add(ebifryIngredient);

        //On ajoute l'ebifry l'assemblage
        prepareRecette.setOrdreAssemblageEffectue(ingredients);

        Assert.assertEquals(ingredients, prepareRecette.getOrdreAssemblageEffectue());
    }

    @Test
    public void estPreteTestEstFalse(){

        //Récupération du livre des recettes et ingrédients
        Livre<Recette> recettes = new Livre<>("recette");
        Livre<Ingredient> livreIngredients = new Livre<>("ingredient");
        Recette ebifry = null;
        Ingredient ebifryIngredient = null;

        //On récupère la recette ebifry
        for (Recette r: recettes.getLivre()) {
            if (r.getNom().equals("ebifry")){
                ebifry = r;
            }
        }

        //On récupère l'ingredient ebifry brut (non cuit)
        for (Ingredient i: livreIngredients.getLivre()) {
            if (i.getNom().equals("ebifry")){
                ebifryIngredient = i;
            }
        }

        //On prepare la recette
        PrepareRecette prepareRecette = new PrepareRecette(ebifry);

        //On vérifie que la recette en préparation n'est pas prête
        Assert.assertFalse(prepareRecette.estPrete());

        //On ajoute un l'ebifry non cuit à l'assemblage
        prepareRecette.addIngredientInListAssemblageEffectue(ebifryIngredient);

        //On vérifie que la recette en préparation n'est toujours pas prête
        Assert.assertFalse(prepareRecette.estPrete());

    }


    @Test
    public void estPreteTestEstTrue(){

        //Récupération du livre des recettes et ingrédients
        Livre<Recette> recettes = new Livre<>("recette");
        Livre<Ingredient> livreIngredients = new Livre<>("ingredient");
        Recette ebifry = null;
        Ingredient ebifryIngredient = null;

        //On récupère la recette ebifry
        for (Recette r: recettes.getLivre()) {
            if (r.getNom().equals("ebifry")){
                ebifry = r;
            }
        }

        //On récupère l'ingredient ebifry brut (non cuit)
        for (Ingredient i: livreIngredients.getLivre()) {
            if (i.getNom().equals("ebifry")){
                ebifryIngredient = i;
            }
        }

        //On prepare la recette
        PrepareRecette prepareRecette = new PrepareRecette(ebifry);

        //On cuit l'ebifry
        ebifryIngredient.setEstCuit(true);

        //On ajoute un l'ebifry cuit à l'assemblage
        prepareRecette.addIngredientInListAssemblageEffectue(ebifryIngredient);


        //On vérifie que la recette en préparationest prête
        Assert.assertTrue(prepareRecette.estPrete());


    }
}
