package model.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test de la class Ingredient
 * Coverage Ingredient
 * Class 100%
 * Method 91% (la méthode main n'est pas testé)
 * Line 96 %  (la méthode main n'est pas testé)
 */
public class IngredientUnitTest {

    @Test
    public void creeAllIngredientsTest(){

        //On crée toutes les ingredients
        ArrayList<Ingredient> ingredients = Ingredient.creeAllIngredients();

        //On vérifie la taille
        Assert.assertEquals(4, ingredients.size());

        //On vérifie l'ordre et le nom
        Assert.assertEquals("riz", ingredients.get(0).getNom());
        Assert.assertEquals("saumon", ingredients.get(1).getNom());
        Assert.assertEquals("algue", ingredients.get(2).getNom());
        Assert.assertEquals("ebifry", ingredients.get(3).getNom());

    }

    @Test
    public void equalsTest(){

        //On crée toutes les ingredients
        ArrayList<Ingredient> ingredients = Ingredient.creeAllIngredients();

        //On prend un ingrédient
        Ingredient ingredient = ingredients.get(0);

        //On crée un ingrédient identique pour le equals que nous avons réécris
        Ingredient ingredient1 = new Ingredient("riz", 5 , false, true);

        //On test la méthode equals
        Assert.assertTrue(ingredient.equals(ingredient1));

    }

    @Test
    public void getterTest(){

        //On crée toutes les ingredients
        ArrayList<Ingredient> ingredients = Ingredient.creeAllIngredients();

        //On prend un ingrédient
        Ingredient ingredient = ingredients.get(0);

        //On test les getters
        // est coupé ?
        Assert.assertTrue(ingredient.isEstCoupe());
        // temps de cuissson
        Assert.assertEquals(5, ingredient.getTempsCuisson());
        // est cuit ?
        Assert.assertFalse(ingredient.isEstCuit());
        // est en cuisson ?
        Assert.assertFalse(ingredient.isEnCuisson());
        // le nom
        Assert.assertEquals("riz", ingredient.getNom());

    }

    @Test
    public void setterTest(){

        //On crée toutes les ingredients
        ArrayList<Ingredient> ingredients = Ingredient.creeAllIngredients();

        //On prend un ingrédient
        Ingredient ingredient = ingredients.get(0);

        //On test les setters
        //set est coupé
        ingredient.setEstCoupe(false);
        Assert.assertFalse(ingredient.isEstCoupe());
        //set est cuit
        ingredient.setEstCuit(true);
        Assert.assertTrue(ingredient.isEstCuit());

    }
}
